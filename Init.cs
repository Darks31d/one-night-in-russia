using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class initGame
    {

        public static Game Game;
        public static void Main(string[] args)
        {
            Game currentGame = new Game();

            Game = currentGame;

            currentGame.Initialize();

            while(currentGame.isRunning == true)
            {
                currentGame.Update();
            }
        }
    }
}