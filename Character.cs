using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class Character
    {
        public List<string> charNameList = new List<string>(); /// Turn this into a list maybe so they can have multiple names and it doesn't have to be exact?
        
        public int charID;

        public Dialogue charDialogue = new Dialogue();

        public bool isInDialogue = false;

        public bool canTalk = true;

        public void createChar(List<string> l, int i)
        {
            charNameList = l;
            charID = i;
        }
    }
}