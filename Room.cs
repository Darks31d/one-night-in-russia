using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class Room
    {

        public string roomDesc;

        public string roomTitle;

        public List<Item> roomItems = new List<Item>();

        public List<Character> roomChar = new List<Character>();

        public List<Exit> exits = new List<Exit>();

        public void setUpRoom(string t, string d)
        {
            roomTitle = t;
            roomDesc = d;
        }

        public void addItemToRoom(Item i)
        {
            roomItems.Add(i);
        }

        public void addCharacterToRoom(Character c)
        {
            roomChar.Add(c);
        }

        public void removeCharacterFromRoom(Character c)
        {
            roomChar.Remove(c);
        }

        public void addExit(Exit exit)
		{
			exits.Add(exit);
		}

		public void removeExit(Exit exit)
		{
			if (exits.Contains(exit))
			{
				exits.Remove(exit);
			}
		}

		public List<Exit> getExits()
		{
			return new List<Exit>(exits);
		}
    }
}