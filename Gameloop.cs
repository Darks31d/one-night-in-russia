using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class Game
    {

        private List<String> directions = new List<String> {"n", "north", "s", "south", "e", "east", "w", "west" }; /// List of all directions, so that I can cordon off those actions
        public bool isRunning = true; /// Is the game running?
        public bool gameOver = false; /// Is the game over? Probably redundant, look to fold that in with running

        public List<int> cordinates = new List<int> { 4, 5 }; /// Player cordinates on a 5x5 grid

        public static List<Room> rooms = new List<Room>(); /// Lit of all rooms on the board. Maybe make a generic one.

        public static List<Character> allChar = new List<Character>();

        public static List<Dialogue.Choice> allChoices = new List<Dialogue.Choice>();

        public static Room currentRoom; /// The current room the player is in

        private bool inDialogue = false;

        public static Player player = new Player();

        public void Initialize() /// First thing to run. Create all the rooms, items, etc, here
        {


            //////////////////////////////////////////////////////////////////////////////////
            ///
            /// THE SETEUP: RED SQUARE, MULTIVERSE, TERMINUS
            ///
            //////////////////////////////////////////////////////////////////////////////////

            /// Starting Desc

            Console.WriteLine("\n\n dPIYb  88b 88 888888     88b 88 88  dPIIb8 88  88 888888     88 88b 88     88IIYb 88   88 .dP'Y8 .dP'Y8 88    db\ndP   Yb 88Yb88 88__       88Yb88 88 dP   `' 88  88   88       88 88Yb88     88__dP 88   88 `Ybo.' `Ybo.' 88   dPYb \nYb   dP 88 Y88 88''       88 Y88 88 Yb  '88 888888   88       88 88 Y88     88'Yb  Y8   8P o.`Y8b o.`Y8b 88  dP__Yb\n YbodP  88  Y8 888888     88  Y8 88  YboodP 88  88   88       88 88  Y8     88  Yb `YbodP' 8bodP' 8bodP' 88 dP''''Yb\n--------------------------------------------------------------------------------------------------------------------\n\n");
            Console.WriteLine("You awaken suddenly in the middle of a wide road. The street is bare and the weather is biting. Looking around, you notice large, regal buildings and one oddly shaped church.\n\nIt all feels vaguely familiar, but you can't remember where you are or how you got there. The only person around is an Old Man, sitting in front of the church. He lifts his head to glance at you before going back to reading his book.\n\nAs you gather your senses, you resolve to find out what's going on. But where to even start...\n\n");
             
            
            ///////////////////
            /// RED SQUARE
            ///////////////////

            Room redSquare = new Room(); /// Creates a room
            redSquare.setUpRoom("The Square", "A large, open square made up of a wide road. There's an Old Man reading a Bible in front of a church, but not much else. The only exit you see is to the North.\n\nIt all looks vaguely familiar"); /// Gives it its cords, title, and desc
            rooms.Add(redSquare); /// Add to room list

            Item basils = new Item();
            List<String> basilNames = new List<String> { "St. Basils", "church", "the church", "basils", "st basils", "st. basils" };
            basils.initItem(basilNames, 7, "A grand, ornate cathedral. It's 9 multi colored domes shimmer in the sun. You're a little bit in awe.");
            redSquare.addItemToRoom(basils);

            Character vlad = new Character();
            List<String> vladNames = new List<String> {"old man", "vlad", "vladimir", "vladimir the great", "old dude" };
            vlad.createChar(vladNames, 1);
            allChar.Add(vlad); 
            Dialogue.Choice vladChoice1 = new Dialogue.Choice("\nThe Old Man looks up from his book. It's clear he's not going to speak first.");
            Dialogue.Choice vladChoice2 = new Dialogue.Choice("'My name is Vladimir.'");
            Dialogue.Choice vladChoice3 = new Dialogue.Choice("He gives you a glare. 'No, I'm not that Vlad. Do I look like I impale people?'\nHe sighs and goes back to his book.");
            Dialogue.Choice vladChoice4 = new Dialogue.Choice("'Don't have one. Didn't need one either. Everyone knew who I was back in my day.'");
            Dialogue.Choice vladChoice5 = new Dialogue.Choice("'They used to call me 'The Great', although I did find it rather pretentious. I wish they had given that love to God instead.'");
            Dialogue.Choice vladChoice6 = new Dialogue.Choice("'The Kieven Rus.'\nHe sees your blank expression and rolls his eyes.\n'Do you not study history young man? Do you not even know your own ancestors? We waded in a river so that you may have salvation. The least you could do is thank us by remembering us. It was really cold river too...'\nHe mumbles something about not being stupid enough to build a capital on a swamp before falling back into silence, lost in his book.");
            Dialogue.Choice vladChoice7 = new Dialogue.Choice("'Not really.'\nHe looks wistfully out over the magnificent buildings lining the grand road.\n'This came from me, but I'm not really from it, if that makes any sense...'");
            Dialogue.Choice vladChoice8 = new Dialogue.Choice("'I built my people, the Russian people, I great kingdom. I brought us from out of the darkness of Paganism, slayed those that opposed our God, and gave us the gift of everlasting life. But do they remember my sacrifices for these things? Do they hold it as dear as the Lord holds his children? No! They throw it away for Western ideals, for 'Enlightenment' or 'Socialism'. What a shame, what a waste. Throwing away your own culture and history for imported ideas. It was so much simpler in Kiev, but I can't even go there anymore...'\n\nHe seems to be lost in his anger. You feel like you should change the topic.");
            Dialogue.Choice vladChoice9 = new Dialogue.Choice("The question seems to light a firy in his eyes.\n'Do you really not reconize the Holy Book?'");
            Dialogue.Choice vladChoice10 = new Dialogue.Choice("He seems annoyed.\n'You're in Russia.'");
            Dialogue.Choice vladChoice11 = new Dialogue.Choice("'Yes, Moscow to be exact. Although I would much rather be in Kiev. Is this not where you're supposed to be?'");
            Dialogue.Choice vladChoice12 = new Dialogue.Choice("'Well, I don't think I'm supposed to be either. But for some reason it feels right to be sitting here. Hm.'\nHe goes back to reading his book.");
            Dialogue.Choice vladChoice13 = new Dialogue.Choice("'Ugh. Lord, what has happened to my land.'\nHe looks wistfully up to the sky before returning back to Earth.\n'Well I must say I am disappointed'");
            Dialogue.Choice vladChoice14 = new Dialogue.Choice("The anger reappears in his eyes\n'I would not speak ill about this 'stupid book'. It gave me my sight, gave me my wife, and built the land you stand on! Now, if you're going to continue to insult the Truth, I think this conversation might be over'\nHe turns in a huff. It's clear you're going to have to engange him again to continue this conversation, although you don't think you should re-approach this topic.");
            Dialogue.Choice vladChoice15 = new Dialogue.Choice("'Hmph. Sure.'\nHe lowers his head and goes back to reading, before raising it once again.\n'To be honest, I was about to send you away but I don't think it would be the holy thing to do. I'll tell you what, if you prove you're one of my Brothers by naming all 12 disciples of our Lord, I will help you out.'");
            Dialogue.Choice vladChoice16 = new Dialogue.Choice("He seems confused, but the scowl slowly turns to a smile\n'You know, I think that truly was the right answer.'\nHe turns back to look at the Church behind him, savoring it's beauty.\n'I picked our religion for its joy, the pageantry, the service that seemed to create heaven on Earth. We can't quantify our love for God in how many saints we've memorized, but I think our Western brothers would disagree. But what do they know, they never had to jump into a river...'\nHe gets lost in his memories before reaching for his book, thrusting it out before him.\n'Here, take it. I think you've earned it.'\nAnd with that, he slips another one out from under his robes and continues reading.\n\nYou realize he had been covering up an inscription on the first page\n\n'To Vladimir, from your grandmother. Love, Olga'");
            Dialogue.Choice vladChoice17 = new Dialogue.Choice("'Hmph. As I thought. I've dealt with enough unruly pagans in my life and I have no interest in suffering through one more. Good day.'\nGrouchier then ever, he turns back to his book.");
            Dialogue.Choice vladChoice18 = new Dialogue.Choice("'Yes, I'm sure I can't turn into a freaking bat. Are you just going to annoy me all day?'");
            Dialogue.Choice vladChoice19 = new Dialogue.Choice("'Hmph. As I thought. I've dealt with enough unruly pagans in my life and I have no interest in suffering through one more. Good day.'\nGrouchier then ever, he turns back to his book.");
            Dialogue.Choice vladChoice20 = new Dialogue.Choice("'Hmph. As I thought. I've dealt with enough unruly pagans in my life and I have no interest in suffering through one more. Good day.'\nGrouchier then ever, he turns back to his book.");
            Dialogue.Choice vladChoice21 = new Dialogue.Choice("He doesn't even look up from his book. You don't think you will speak again. You feel sad.");
            allChoices.AddRange(new List<Dialogue.Choice>() { vladChoice1, vladChoice2, vladChoice3, vladChoice4, vladChoice5, vladChoice6, vladChoice7, vladChoice8, vladChoice9, vladChoice10, vladChoice11, vladChoice12, vladChoice13, vladChoice14, vladChoice15, vladChoice16, vladChoice17, vladChoice18, vladChoice19, vladChoice20, vladChoice21 });

            vlad.charDialogue.Connect(vladChoice1, vladChoice2, "Who are you?");
            vlad.charDialogue.Connect(vladChoice2, vladChoice3, "Wait, you're a vampire!?!");
            vlad.charDialogue.Connect(vladChoice3, vladChoice18, "Huh, are you sure? Because you kind of look like that Vlad.");
            vlad.charDialogue.Connect(vladChoice18, vladChoice4, "No, I'm sorry... Uh, do you have a last name?");
            vlad.charDialogue.Connect(vladChoice2, vladChoice4, "What's your last name?");
            vlad.charDialogue.Connect(vladChoice4, vladChoice5, "So what'd they call you?");
            vlad.charDialogue.Connect(vladChoice5, vladChoice6, "Who's 'they'?");
            vlad.charDialogue.Connect(vladChoice6, vladChoice9, "Why do you care so much about some stupid book? I'm trying to talk to you!");
            vlad.charDialogue.Connect(vladChoice2, vladChoice7,"Are you from around here?");
            vlad.charDialogue.Connect(vladChoice7, vladChoice8, "What do you mean?");
            vlad.charDialogue.Connect(vladChoice8, vladChoice4, "Uh... Hey, I never got your last name.");
            vlad.charDialogue.Connect(vladChoice1, vladChoice10, "Do you know where we are?");
            vlad.charDialogue.Connect(vladChoice10, vladChoice11, "Wait, what?!");
            vlad.charDialogue.Connect(vladChoice11, vladChoice7, "Are you from here? I think I might need directions.");
            vlad.charDialogue.Connect(vladChoice11, vladChoice12, "I don't think so.");
            vlad.charDialogue.Connect(vladChoice12, vladChoice9, "What are you reading?");
            vlad.charDialogue.Connect(vladChoice9, vladChoice13, "No?");
            vlad.charDialogue.Connect(vladChoice13, vladChoice14, "Why do you care so much about some stupid book?");
            vlad.charDialogue.Connect(vladChoice9, vladChoice15, "Oh yeah that one, of course!");
            vlad.charDialogue.Connect(vladChoice15, vladChoice17, "Simon Peter, Andrew, James, John, Philip, Bartholomew, Thomas, Matthew, James, Thaddaeus, Simon, Judas Iscariot");
            vlad.charDialogue.Connect(vladChoice15, vladChoice19, "Peter, Andrew, James, John, Philip, Leo, Thomas, Matthew, James, Thaddaeus, Simon, Judas Iscariot");
            vlad.charDialogue.Connect(vladChoice15, vladChoice20, "Peter, Andrew, James, John, Philip, Leo, Clement, Matthew, James, Thaddaeus, Simon, Judas Iscariot");
            vlad.charDialogue.Connect(vladChoice15, vladChoice16, "To be honest, I don't know. I only really went to church for the atmosphere");
            vlad.charDialogue.Connect(vladChoice1, vladChoice9, "What are you reading?");

            vlad.charDialogue.origHead = vladChoice1;
            redSquare.addCharacterToRoom(vlad);

            //////////////////////
            /// MULTIVERSE
            //////////////////////

            Room multiverse = new Room();
            multiverse.setUpRoom("The Precipice", "As you enter this wide bolevard, you're struck by the architecture that lines the street. Giant concrete apartments tower over thatched roof huts, which stands next to regal Imperial dachas. It's almost as if all of history has collided into this one place. You're only a few steps in when you're suddenly feel your legs go limp. As you shake and start to fall over, you suddenly hear a voice from above.\n\n'Take the Revolutionaries Gun.\nKill the Last King\nSave Your Country'\n\nYour sight is restored and you jump up to your feet, but it's too late. Whoever just spoke to you is gone, but you think you just got your instructions for what to do here. Gathering your senses, you realize the only exit is to the north. Above it reads a sign:\n\n'The Precipice;");
            rooms.Add(multiverse);

            /////////////////////////
            /// THE ROUNDABOUT
            /////////////////////////

            Room terminus  = new Room();
            terminus.setUpRoom("The Roundabout", "A large park, filled with idylic meadows and little creeks. It's surrounded by a wide roundabout with exits to the North, West, and East.The only other person around appears to be a garderner, planting some red flowers.");
            rooms.Add(terminus);

            //////////////////////////////////////////////////////////////////////////////////
            ///
            /// LITERATURE LANE: SHARIK, PROF, AND SOFIA
            ///
            //////////////////////////////////////////////////////////////////////////////////

            //////////////////////
            /// BULGAKOV LANE
            //////////////////////

            Room bulgakovLane = new Room();
            bulgakovLane.setUpRoom("Bulgakov Lane", "A narrower street with a sign that reads 'Bulgakov Lane'. There isn't much here except a butchershop. The only exit is to the East, leading back to the roundabout.\n\nA mysterious gentleman stands outside of it, holding a package. He takes no notice of you and continues to hum a tune underneath his breath.\n'towards the sacred banks of the Nile...'");
            rooms.Add(bulgakovLane); /// Add to room list

            Character prof = new Character();
            List<String> profNames = new List<String> {"mysterious gentleman", "hummer", "professer", "the professer", "gentleman", "the gentleman"};
            prof.createChar(profNames, 2);
            allChar.Add(prof);

            Dialogue.Choice profChoice1 = new Dialogue.Choice("Clearing your throat knocks him out of his own world and back to reality. He turns to look at you inquisitively.\n'Do I know you?'");
            Dialogue.Choice profChoice2 = new Dialogue.Choice("'Hm. I could have sworn I had done some work on you before'\n'He continues to hum\n'From Seville and to Granada...");
            Dialogue.Choice profChoice3 = new Dialogue.Choice("'Well you look so fresh, so young. I assumed you had a little help from science. No matter though, this is much more interesting...'\nHe looks you up and down, like a predator studying his prey.\n'Hm, if I did not do this, I do wonder who did... Are you free now by any chance? I'd love to continue this conversation in my apartment. I have a lovely dining room if you'd like something to eat. Well, if it hasn't been taken from me yet.");
            Dialogue.Choice profChoice4 = new Dialogue.Choice("'Ah wonderful, I'm so happy you are willing to help advance science. Please, just come this way.'\nYou turn to follow his outstretched hand when a cloth is clamped over your mouth. You try to struggle, but you feel weak and slump to the ground...\n\nWhat comes next is too blurry to focus on, but you do remember a few distint things: the sharpness of the scapel, the blinding lights, the sterile coldness of the room. And floating above it all, that haunting melody and the last thing you'll hear:\n\n'Towards the sacred banks of the Nile...'");
            Dialogue.Choice profChoice5 = new Dialogue.Choice("He eyes narrow.\n'Hm, you live down the street yet I don't think I've seen you before. From your clothes, you're obviously not a member of respectable society, which means you couldn't afford an apartment in one of these hallowed houses, which means...'\nHe steps back and flicks his hand.\n'Get lost, I already spoke to your 'comrades' on the housing comittee earlier and I have no use for magazines except to use as toliet paper!'");
            Dialogue.Choice profChoice6 = new Dialogue.Choice("'You mean you're not one of those vultures?'");
            Dialogue.Choice profChoice7 = new Dialogue.Choice("'Ugh, just when I thought I had managed to escape your grasp.'\nHe shakes his head and opens the package he has been holding. It's full of sausage.\n'Here, take one of these and never bother me again, or I'll sick the central commitee onto you! Now leave!'\n\nHe angrily shoves the gift into your hands and pulls down the brim of his hat, once again lost in his own thoughts. As you turn to leave, you hear him softly sing.\n\n'From Seville to Granada...'");
            Dialogue.Choice profChoice8 = new Dialogue.Choice("'Oh, my mistake. I'm so sorry for comparing you to those animals. You can't be too careful nowadays...'");
            Dialogue.Choice profChoice9 = new Dialogue.Choice("Just some sausage for a friend. Krakow sausage too. He'll be very happy, they haven't had them in stock forever. Although I can't be too loud about it, lest some new comitee come to 'redistribute' it. Alas, alas...");
            Dialogue.Choice profChoice10 = new Dialogue.Choice("He lets out a sharp laugh.\n'Do I look like I'm stupid. I am not like those zealots on the Central Commitee. No, no, you will get nothing for free, I've got to pay for a 7 room apartment somehow. However, if you have a few kopeks, I think I can let a piece or two go.");
            Dialogue.Choice profChoice11 = new Dialogue.Choice("He slowly looks you up and down.\n'Hm, I am feeling generous. Follow me, please, and I'm sure we can work something out.\nYou turn to follow his outstretched hand when a cloth is clamped over you mouth. You feel dizzy and before you know it, you've hit the ground..\n\nWhat comes next is too blurry to focus on, but you do remember a few things: the sharpness of the scalpel, the stench of anesthesia, the sterile coldness of the room. And above it all, that haunting melody, the last thing you'll here.\n\n'Towards the sacred banks of the Nile...'");
            Dialogue.Choice profChoice12 = new Dialogue.Choice("'Hmph, no. Not for any particular reason, mind you, I just don't want to. Despite what the government might want, I do still have some free will. Come back when you can pay.'");
            Dialogue.Choice profchoice13 = new Dialogue.Choice("He pays you no attention, lost in his world. It's clear he's not going to talk to you. As you turn away, you hear him softly hum.\n\n'Towards the sacred banks of the Nile...");

            Dialogue.Choice profChoice14 = new Dialogue.Choice("'Ah my beautiful friend has returned! Do you have the money?");
            Dialogue.Choice profchoice15 = new Dialogue.Choice("'Hmph. What a shame.\nHe turns away once gain.'");
            Dialogue.Choice profChoice16 = new Dialogue.Choice("'Well, now we're talking. Thank you for conducting buisness like a civilized gentleman.'\nHe breaks you off a piece off sausage, takes your money, and once again becomes lost in his own world.\n'Towards the sacred banks of the Nile...'");
            allChoices.AddRange(new List<Dialogue.Choice>() { profChoice1, profChoice2, profChoice3, profChoice4, profChoice5, profChoice6, profChoice7, profChoice8, profChoice9, profChoice10, profChoice11, profChoice12, profchoice13, profChoice14, profchoice15, profChoice16 });

            prof.charDialogue.Connect(profChoice1, profChoice2, "No I don't think I do.");
            prof.charDialogue.Connect(profChoice2, profChoice3, "Some work done?");
            prof.charDialogue.Connect(profChoice3, profChoice4, "Sure. Where is it?");
            prof.charDialogue.Connect(profChoice3, profChoice9, "Uhh, What's in the package?");
            prof.charDialogue.Connect(profChoice1, profChoice5, "Uh... Maybe, I live down the street.");
            prof.charDialogue.Connect(profChoice5, profChoice6, "Housing Commitee?");
            prof.charDialogue.Connect(profChoice6, profChoice8, "No?");
            prof.charDialogue.Connect(profChoice6, profChoice7, "Uh.. Yes, I am, and I'll have you know I just heard back from the Central Commitee. They're very displeased with you!");
            prof.charDialogue.Connect(profChoice8, profChoice9, "What do you have there?");
            prof.charDialogue.Connect(profChoice9, profChoice10, "I could take some off of your hands if you're worried about that. There's a dog down the street that looks quite hungry.");
            prof.charDialogue.Connect(profChoice10, profChoice11, "Is there another way I could pay?");
            prof.charDialogue.Connect(profChoice10, profChoice12, "Don't you want to help a stranger in need?");

            prof.charDialogue.Connect(profChoice14, profchoice15, "No, not yet.");
            prof.charDialogue.origHead = profChoice1;
            bulgakovLane.addCharacterToRoom(prof);

            //////////////////////
            /// KALABUKHOV HOUSE
            //////////////////////

            Room kalabukhovHouse = new Room();
            kalabukhovHouse.setUpRoom("Obukhov Lane", "A nicer neighborhood filled with flowers and trees. Its wide bolevard has good looking apartment buildings lining each side. You spy a small stray dog lying outside one of them, a more run down house with a broken window. There's a suspicious hole, covered in dirt, in front of it. There's an exit to the East, going back to the roundabout, and one to the North.");
            rooms.Add(kalabukhovHouse);

            Character sharik = new Character();
            List<String> sharikNames = new List<String> {"dog", "the dog", "sharik", "stray", "sharikov"};
            sharik.createChar(sharikNames, 3);
            allChar.Add(sharik);

            Dialogue.Choice sharikChoice1 = new Dialogue.Choice("\nThe dog looks up and gives you a look. Besides the burn on his side, he looks pretty healthy. He lets out a short bark.");
            Dialogue.Choice sharikChoice2 = new Dialogue.Choice("He gives a quick help before panting repeadetly. It doesn't look like he's eaten for some time.");
            Dialogue.Choice sharikChoice3 = new Dialogue.Choice("He whines and glumly drops his head back into his paws.");
            Dialogue.Choice sharikChoice4 = new Dialogue.Choice("He is clearly grateful and lets out some happy barks before taking the sausage and running off. You're about to leave before you notice him pawing and scraping at the pile of dirt in front of the House. He pulls out a worn and dirty letter before running back and, to your amazement, standing up on his hind legs to give it to you. You take it despite your confusion and the dog walks away before plopping back down on all 4s. Slightly unsettled, you wipe the slobber off of the letter and turn it over. It reads 'From Kolya, Camp 12'");
            Dialogue.Choice sharikChoice5 = new Dialogue.Choice("Walking up to the dog just makes him lazily roll over and stick out his tongue, expecting more meat. You go to give him a pat on the belly and move on. You notice he has a large scar there.");
            allChoices.AddRange(new List<Dialogue.Choice>() { sharikChoice1, sharikChoice2, sharikChoice3, sharikChoice4, sharikChoice5 });

            sharik.charDialogue.Connect(sharikChoice1, sharikChoice2, "Hey little ball, do you want something?");
            sharik.charDialogue.Connect(sharikChoice2, sharikChoice3, "Sorry little ball, I don't have anything for you.");
            /// Add in a connection to this when the prof gives you the sausage.
            sharik.charDialogue.origHead = sharikChoice1;

            kalabukhovHouse.addCharacterToRoom(sharik);

            Item hole = new Item();
            List<String> holeNames = new List<String> { "hole", "big hole", "covered hole", "dirt" };
            hole.initItem(holeNames, 4, "A hole in the ground, covered in dirt. There looks to be something in it.");
            kalabukhovHouse.addItemToRoom(hole);

            Item profHouse = new Item();
            List<String> profHouseNames = new List<String> { "Kalabukhov House", "house", "broken house", "broken window", "professers house", "the house" };
            profHouse.initItem(profHouseNames, 8, "A rather large apartment building who's best days have already passed. You see see a kitchen through its broken window in the front. It looks like water is seeping out into it from a bathroom.");
            kalabukhovHouse.addItemToRoom(profHouse);

            //////////////////////
            /// THE BLOCK
            //////////////////////

            Room theBlock = new Room();
            theBlock.setUpRoom("The Block", "The elegant brownstone apartments have faded away, replaced with hulking concrete monstrosities that overshadow the street. Dirt cakes the windows, Propaganda Posters line the walls, and gloom seems to seep out of every open crevice. The only person in this sad seen appears to be a woman, pacing in front of the entrance nervously. The only exit is to the South.");
            rooms.Add(theBlock);

            Character sofia = new Character();
            List<String> sofiaNames = new List<String> { "sofia", "woman", "pacing woman", "sofia petrovna" };
            sofia.createChar(sofiaNames, 4);
            allChar.Add(sofia);

            Item agiProp = new Item();
            List<String> agiPropNames = new List<String> { "Propaganda Posters", "propaganda posters", "posters", "propaganda", "the propaganda posters" };
            agiProp.initItem(agiPropNames, 9, "Fraying posters on the wall that glorify the Revolution. One in particular catches your eye. It's a poster of a man's face superimposed on top of a sun. He appears to be looking straight at you.");
            theBlock.addItemToRoom(agiProp);

            Dialogue.Choice sofiaChoice1 = new Dialogue.Choice("'Excuse me, are you alright?'\nShe looks up at you with wide, scared eyes.\n'Oh, oh yes, I'm fine comrade. All fine. I had just come from the Party office. My son is getting a promotion soon and I needed to fill out some paperwork...'\nHer voice trails off and she again looks at the ground. You feel like you're talking with a scared little animal more than a person.");
            Dialogue.Choice sofiaChoice2= new Dialogue.Choice("That just seems to scare her more then comfort her.\n'Yes I said everything was fine so everything is fine! No complaints here. It just wish that office I went to was more thorough, they brushed me off like I was nothing! I need to write another letter to Him, He'll get it fixed that's for sure!'");
            Dialogue.Choice sofiaChoice3 = new Dialogue.Choice("She looks taken back\n'Why, he's the reason we're all here! The reason why we have all these wonderful things!'\nShe gestures to the worn down, dirty street and the creaking concrete monstrosity behind here.\n'All this space used to be one home. Now it's a home for a hundred! Sure, it needs some cleaning up, but He'll get it done. That's why we follow Him after all, He always makes it better.'");
            Dialogue.Choice sofiaChoice4 = new Dialogue.Choice("Her jaw nearly hits the cracked stones below.\nOf course I'm happy! I have a wonderful job, a wonderful life, and...'\nHer voice catches. The warmth of anger seems to slowly seep from her eyes as she forces herself to finish the sentence.\n'...and, and a wonderful son. The best son..'n");
            Dialogue.Choice sofiaChoice5 = new Dialogue.Choice("Her eyes go even wider. You didn't think that was possible.\n'No, no! I said it wasn't a complaint, it wasn't!'\nHer eyes dart around quickly, scoping out every dark corner, as she continues to rant loudly.\n'No I'd never complain about our glorious system. There's nothing wrong with it at all!'");
            Dialogue.Choice sofiaChoice6 = new Dialogue.Choice("She takes one wide look around before diving in, inches from your face. You can hear the panic in her voice.\n'You don't understand, there's enemies in our mist. They're trying to overthrow Him, take it all away. They grabbed my son by mistake and if they think I'm involved I'll never be able to get him out. Please, please just let me be!");
            Dialogue.Choice sofiaChoice7 = new Dialogue.Choice("Her eyes light up at the mere mention of her son.\n'Oh, it's wonderful! His loyalty has been rewarded by our Dear Leader and he'll soon be taking us to the Crimea with him! Myself and his new wife that is. She's a real beauty, I'm so very excited for the wedding. My little Kolya, all grown up...'\nThe tiniest of smiles appears on her lips.");
            Dialogue.Choice sofiaChoice8 = new Dialogue.Choice("And just like that, the fear returns.\n'No, no, he's very busy my Kolya. So very busy. He hasn't even been able to come home for a while because of'\nShe chokes, unable to finish the sentence. You're unsettled.");
            Dialogue.Choice sofiaChoice9 = new Dialogue.Choice("Her face is as pale as a corpse and she just bursts into tears.\n'Oh, it's horrible, so horrible. My only love left on this Earth, my pride and joy, and he's taken away from me over a horrible mistake. I'm made a failure of a mother for a crime that couldn't have been committed. Why would he try to overthrow the person he loved? No, no, it makes no sense!'");
            Dialogue.Choice sofiaChoice10 = new Dialogue.Choice("'No, no, NO! That's what I've been trying to tell them! My Kolya couldn't have betrayed Him, the thought never would have crossed his mind! He loved His system as much as he loved me. My son has been caught up in some grand mistake, that is all. I just need to write Him again and He'll fix it all, just you watch'");
            Dialogue.Choice sofiaChoice11 = new Dialogue.Choice("She looks aghast.\n'What are you trying to say? That my son is a terrorist? Some sort of 5th Column?!?'");
            Dialogue.Choice sofiaChoice12 = new Dialogue.Choice("She says nothing, just staring with her mouth open. The reality she has constructed seems to be crashing down all around her. All she can do is turn around and sit down on the stoop, holding back tears. You wait a few minutes for her to continue your pleasant conversation but it appears as if you won't speak again. As you turn away, you hear a soft voice.\n'Kolya, my Kolya...'");
            Dialogue.Choice sofiaChoice13 = new Dialogue.Choice("'Yes, yes! That's what I've been telling all of them! He has given us this wonderful new world, I know he will give me back my son when he hears about his amazingness.'\nShe still sniffles but the tears have begun to dry. She quickly wipes the rest away.\n'I don't think I realized how much I needed to hear that. It's so hard these days with everything going on to make sense of it all. It's nice to hear someone believe in me. So, thank you'\n'The little smile appears again.\n'Now that I know you're a friend, would you mind doing me a favor? My son was supposed to write me today, but the postal carrier told me he lost his postal bag when he was attacked by some feral beast of a dog! If you see it, could you bring it back here for me?'\nYou voice stands strong, but you can hear her pleading silently for your answer.");
            Dialogue.Choice sofiaChoice14 = new Dialogue.Choice("'Oh thank you, thank you! May God bless you!'\nShe gives you a quick hug before once again returning to her apartment, lost once again in her blissful fantasy.");
            Dialogue.Choice sofiaChoice15 = new Dialogue.Choice("The chattering stops and the world seems to slow to a halt for her. She gingerly lifts her head and looks you right in the eyes. For the first time, you see something within them: Hope.\n'Yes, he is. How did you know?'");
            Dialogue.Choice sofiaChoice16 = new Dialogue.Choice("And just like that, she explodes into a ball of energy.\n'Oh, oh thank you, thank you so much! He did read my letters after all! Praise the System, Praise Him! May I please see it? I'm dying to know how my darling boy is.'");
            Dialogue.Choice sofiaChoice17 = new Dialogue.Choice("The smile stays frozen on her face as she processes what you have just said. It slowly works its way through her stress addled brain: No getting out of this, no pleading to Him, no seeing her son again. And all of the pain inflected on her boils up and out.\n'no, no, No, No, NO!'\nShe shrieks right in your face before running off. It doesn't look like this was the right choice. You turn to leave and begin walking away before you're suddenly on the ground. You're head throbs and you spy the blood strained brick that was just thrown at your head. You try to get back up by your eyesight rapidly begins to falter, and the last thing you see is that poor, battered women pick up her letter and flee.\n\nEveryone has their limit.");
            Dialogue.Choice sofiaChoice18 = new Dialogue.Choice("TODO: She's really happy and gives you the necklace");

            Dialogue.Choice sofiaChoice19 = new Dialogue.Choice("'Have you managed to find them yet, by any chance?'\nShe waits on pins and needles for your answer.");
            Dialogue.Choice sofiaChoice20 = new Dialogue.Choice("The vigor seems to drain out of her instantly.'Oh, alright'\nAnd with that, she goes back to her own fantasy world.");
            Dialogue.Choice sofiaChoice21 = new Dialogue.Choice("And just like that, she explodes into a ball of energy.\n'Oh, oh thank you, thank you so much! He did read my letters after all! Praise the System, Praise Him! May I please see it? I'm dying to know how my darling boy is.'");
            Dialogue.Choice sofiaChoice22 = new Dialogue.Choice("The smile stays frozen on her face as she processes what you have just said. It slowly works its way through her stress addled brain: No getting out of this, no pleading to Him, no seeing her son again. And all of the pain inflected on her boils up and out.\n'no, no, No, No, NO!'\nShe shrieks right in your face before running off. It doesn't look like this was the right choice. You turn to leave and begin walking away before you're suddenly on the ground. You're head throbs and you spy the blood strained brick that was just thrown at your head. You try to get back up by your eyesight rapidly begins to falter, and the last thing you see is that poor, battered women pick up her letter and flee.\n\nEveryone has their limit.");
            Dialogue.Choice sofiaChoice23 = new Dialogue.Choice("TODO: She's really happy and gives you the necklace");

            Dialogue.Choice sofiaChoice24 = new Dialogue.Choice("As you approach her, she notices and just adjusts her walking to avoid you. She won't even meet your eye. It's clear she won't speak to you anymore.");

            allChoices.AddRange( new List<Dialogue.Choice>() { sofiaChoice1, sofiaChoice2, sofiaChoice3, sofiaChoice4, sofiaChoice5, sofiaChoice6, sofiaChoice7, sofiaChoice8, sofiaChoice9, sofiaChoice10, sofiaChoice11, sofiaChoice12, sofiaChoice13, sofiaChoice14, sofiaChoice15, sofiaChoice16, sofiaChoice17, sofiaChoice18, sofiaChoice19, sofiaChoice20, sofiaChoice21, sofiaChoice22, sofiaChoice23, sofiaChoice24 } );

            sofia.charDialogue.Connect(sofiaChoice1, sofiaChoice2, "Are you sure you're alright? You seem a little frazzled.");
            sofia.charDialogue.Connect(sofiaChoice2, sofiaChoice3, "Who's 'Him'?");
            sofia.charDialogue.Connect(sofiaChoice3, sofiaChoice4, "I don't know, this seems pretty crappy to me.");
            sofia.charDialogue.Connect(sofiaChoice2, sofiaChoice5, "That sounds like a complaint to me.");
            sofia.charDialogue.Connect(sofiaChoice5, sofiaChoice6, "You can speak freely, I'm not here to trap you");
            sofia.charDialogue.Connect(sofiaChoice1, sofiaChoice7, "Oh that's nice. What position is your son getting?");
            sofia.charDialogue.Connect(sofiaChoice4, sofiaChoice7, "Well, if he's so great, what job is he getting?");
            sofia.charDialogue.Connect(sofiaChoice7, sofiaChoice8, "He sounds like a good man. Will I be able to meet him?");
            sofia.charDialogue.Connect(sofiaChoice8, sofiaChoice9, "Uh, do you need anything?");
            sofia.charDialogue.Connect(sofiaChoice6, sofiaChoice9, "What do you mean they took your son?");
            sofia.charDialogue.Connect(sofiaChoice9, sofiaChoice10, "Wait, he committed a crime?");
            sofia.charDialogue.Connect(sofiaChoice10, sofiaChoice11, "That's a pretty serious charge. I don't think that was a mistake.");
            sofia.charDialogue.Connect(sofiaChoice11, sofiaChoice12, "If the shoe fits...");
            sofia.charDialogue.Connect(sofiaChoice10, sofiaChoice13, "Well, if this guy is as great as you've been saying, I'm sure you'll be able to get him out.");
            sofia.charDialogue.Connect(sofiaChoice13, sofiaChoice14, "Of course, I'll let you know");
            sofia.charDialogue.Connect(sofiaChoice15, sofiaChoice16, "I found this letter down the street, and it's from someone named Kolya. The return address says 'Camp 12'");
            sofia.charDialogue.Connect(sofiaChoice16, sofiaChoice17, "(Lie) Ah ha, I trapped you! Secret Police, I'm going to need a reward or you're coming with me for collaborating with an Enemy of the State");
            sofia.charDialogue.Connect(sofiaChoice16, sofiaChoice18, "Here you go.");

            sofia.charDialogue.Connect(sofiaChoice19, sofiaChoice20, "No, not yet. I'm sorry.");
            sofia.charDialogue.Connect(sofiaChoice21, sofiaChoice22, "(Lie) Ah ha, I trapped you! Secret Police, I'm going to need a reward or you're coming with me for collaborating with an Enemy of the State");
            sofia.charDialogue.Connect(sofiaChoice21, sofiaChoice23, "Here you go.");


            sofia.charDialogue.origHead = sofiaChoice1;

            theBlock.addCharacterToRoom(sofia);


            //////////////////////////////////////////////////////////////////////////////////
            ///
            /// MIDDLE ROW: DIRECTORS, BORIS AND GLEB, NECHAEV, REV WOMEN
            ///
            //////////////////////////////////////////////////////////////////////////////////

            //////////////////////
            /// TATLINS SQUARE
            //////////////////////

            Room tatlinSquare = new Room();
            tatlinSquare.setUpRoom("Tatlin's Square", "A wide square with two outdoor soundstages on each side. A hive of workers are in the center of it, constructing a giant Spire made of shaped steel. It stretches up to near the clouds, twisting it's way to heaven. The construction seems to be supervised by a man in a labcoat who moves around on a floating platform of sorts. Even from a distance you can see his hairy hands.\n\nThe two soundstages are also hives of activity. One is set on the banks of a river, where a mock warship set is set up. The man in charge seems to be an Excited Director, standing on some crates and shouting over the masses. The other set appears to be set up for some sort of swamp scene you can see what appears to be a child actor's stunt double swimming through it. You notice Exits to the South, East, and North.");
            rooms.Add(tatlinSquare);

            Character director = new Character();
            List<String> directorNames  = new List<String> { "excited director", "director", "eisenstein", "the director" };
            director.createChar(directorNames, 6);
            allChar.Add(director);

            Item tatlinsTower = new Item();
            List<String> tatlinsTowerNames = new List<String> { "The Spire", "spire", "the spire", "tatlins tower", "tower", "the tower", "tatlin's tower" };
            tatlinsTower.initItem(tatlinsTowerNames, 11, "A grand, curving tower. A miracle of engineering, you can't believe it's actually still standing. Hundreds of grizzled workers bolt the beams together while a rather strange man on some sort of floating platform goes around directing it. You notice his labcoat and hairy hands.");
            tatlinSquare.addItemToRoom(tatlinsTower);

            

            Dialogue.Choice directorChoice1 = new Dialogue.Choice("The man at the camera peers through his lense, his wild hair going out in all directions. He has the infectious smile of a creative in his element as he shouts out orders to his actors.");
            Dialogue.Choice directorChoice2 = new Dialogue.Choice("He cuts you off with a wave of his hand.\n'If you're trying to deliver a coffee that's not mine, I haven't had anything to eat or drink in days. Much too busy'\nHe doesn't even take his eye out of the lense.");
            Dialogue.Choice directorChoice3 = new Dialogue.Choice("That manages to get his attention. He breaks away from the camera to look at you inquistively, and a little bit suspisciously.\n'Then how did you get onto my set? I told security I didn't want any distractions! This piece is much too important! No, no, this will not do!'\nHe moves like a bird, wild and free.");
            Dialogue.Choice directorChoice4 = new Dialogue.Choice("'Ah, why didn't you say so? You shouldn't waste an artists time you know. Here, take one of these uniforms and just get into the shot. You're going to be a sailor.'\nYou don't know what you've gotten yourself into but you get changed and then make your way out to the set. Apparently the scene you're in is one where some sailors stage a revolt against their officers. It's very frantic as you run around and try to pretend like you belong.\n\nThe director is a creature of energy, constantly bounding about and shouting out orders and commands to make the scene more 'lively' and 'emotional'. You can tell how much he loves his work.\n\nFinally, after what feels like an eternity, he yells 'Cut!'. You collapse to the ground, exhausted, but you notice him looking at you. He waves you over. You don't know what he wants to talk about.");
            Dialogue.Choice directorChoice5 = new Dialogue.Choice("'Oh, perfect, yes perfect! I just wanted to call you over to tell you myself. The little extra who came in late stealing the show. Only in cinema could that happen! Although, if the joy of creating art isn't the reward you want, I think this might suffice.'\n\nHe calls over an assistant and gives you a few kopeks.\n'Here's your pay for the day, my assistant will show you to the exit.'");
            Dialogue.Choice directorChoice6 = new Dialogue.Choice("He laughs with a jolly tone.\n'Come on, cheer up. You're now going to be preserved for all of history, all on this little reel right here. Making cinema is supposed to be a happy thing, even when you're tired. Although, if the joy of creating art isn't your thing, I think this might suffice.'\n\nHe calls over an assistant and gives you a few kopeks.\n'Here's your pay for the day, my assistant will show you to the exit.'");
            Dialogue.Choice directorChoice7 = new Dialogue.Choice("'Oh of course, I love talking with my actors. What's on your mind?'");
            Dialogue.Choice directorChoice8 = new Dialogue.Choice("'Why, this is the greatest artistry center in all the world! The most genius minds in all of Russia come together to create anew, free of the constraints of the Tsarist regime. True creativity finally unleashed!");
            Dialogue.Choice directorChoice9 = new Dialogue.Choice("Well, there's me of course, the great Sergei. There's my fellow brother of film Tarkovsky, and then, well, I can't remember the name of the one helping build our monument. although I'm pretty sure it was D-something. A rather odd fellow, honestly. So caught up in the numbers of it all. I feel I must review my grade school math textbook before I can even understand half the things he says. But he's a grand engineer and has done a wonderful work on the tower so far.");
            Dialogue.Choice directorChoice10 = new Dialogue.Choice("'Oh, it's a wonderful piece about the 1905 Revolution, and how we came together as Russian brothers against the tyranny of the Tsar. The People rising up for the Socialist system, finding our brothers, all of that good stuff. It will be a wonderful piece, I tell you, watched for years to come! Maybe it'll even be studied one day, although I'm a bit too modest to say it'll be that important...'");
            Dialogue.Choice directorChoice11 = new Dialogue.Choice("'How can one not be excited about finally having freedom to do what they love? Soviet filmmaking is taking the field to the frontier and we get to be the Pioneers. It's exhilarating, I tell you.'");
            Dialogue.Choice directorChoice12 = new Dialogue.Choice("'Farewell, brother. And once again, thank you for the wonderful performance.'\nHe turns back to the camera and is once again lost in his world, his heaven. Before you leave, you take note of his smile, like that of a childhood. You realize he's only person you've met so far who is truly satisfied.");
            Dialogue.Choice directorChoice13 = new Dialogue.Choice("He doesn't even look up from his camera. It looks like he's too busy to talk.");
            allChoices.AddRange( new List<Dialogue.Choice>() { directorChoice1, directorChoice2, directorChoice3, directorChoice4, directorChoice5, directorChoice6, directorChoice7, directorChoice8, directorChoice9, directorChoice10, directorChoice11, directorChoice12, directorChoice13  } );

            director.charDialogue.Connect(directorChoice1, directorChoice2, "Excuse me...");
            director.charDialogue.Connect(directorChoice2, directorChoice3, "What? I don't even work here!");
            director.charDialogue.Connect(directorChoice3, directorChoice4, "Uh... I'm an extra. They told me to come over here and talk to you.");
            director.charDialogue.Connect(directorChoice4, directorChoice5, "Was that good enough?");
            director.charDialogue.Connect(directorChoice4, directorChoice6, "I'm not going to do another take, I think I nearly died out there.");
            director.charDialogue.Connect(directorChoice6, directorChoice7, "Actually, do you mind if I ask you some questions first?");
            director.charDialogue.Connect(directorChoice5, directorChoice7, "Actually, do you mind if I ask you some questions first?");
            director.charDialogue.Connect(directorChoice7, directorChoice8, "What is this place?");
            director.charDialogue.Connect(directorChoice8, directorChoice9, "Who else is here?");
            director.charDialogue.Connect(directorChoice7, directorChoice9, "What is your film about?");
            director.charDialogue.Connect(directorChoice11, directorChoice8, "What is this place?");
            director.charDialogue.Connect(directorChoice9, directorChoice10, "What is your film about?");
            director.charDialogue.Connect(directorChoice10, directorChoice11, "You seem excited about it.");
            director.charDialogue.Connect(directorChoice11, directorChoice12, "I think I need to leave now. Thank's for the money.");
            director.charDialogue.Connect(directorChoice9, directorChoice12, "I think I need to leave now. Thank's for the money.");
            director.charDialogue.Connect(directorChoice5, directorChoice12, "I think I need to leave now. Thank's for the money.");
            director.charDialogue.Connect(directorChoice6, directorChoice12, "I think I need to leave now. Thank's for the money.");

            director.charDialogue.origHead = directorChoice1;

            tatlinSquare.addCharacterToRoom(director);

            //////////////////////
            /// THE BRIDGE
            //////////////////////

            Room theBridge = new Room();
            theBridge.setUpRoom("The Bridge", "A tall, wide bridge that appears wells worn. It's 4 lanes and a grand, ornate divider with various statues on it. There doesn't appear to be much of interest except for Two Men who appear to be arguing with each other near the edge. There are Exits to the North and to the South.");
            rooms.Add(theBridge);

            Character bAndG = new Character();
            List<String> bAndGNames  = new List<String> { "two men", "men", "boris and gleb", "boris", "gleb" };
            bAndG.createChar(bAndGNames, 7);
            allChar.Add(bAndG);

            Dialogue.Choice bAndGChoice1 = new Dialogue.Choice("The men continue to shout and scream over each other, paying no attention to you.\n'I can't believe you right now Gleb!'\n'Screw off Boris, let go of me and let me die for you!'");
            Dialogue.Choice bAndGChoice2 = new Dialogue.Choice("Exasperated, they turn to you. One crosses his arms while the other rolls his eyes.\n'What?', they say in unision.");
            Dialogue.Choice bAndGChoice3 = new Dialogue.Choice("They look at each other, attempting to defer to the other person. It goes on for some time before Gleb finally rolls his eyes and speaks.\n'What's it look like we're doing? We're trying to kill ourselves'");
            Dialogue.Choice bAndGChoice4 = new Dialogue.Choice("The one named Boris smacks Gleb in the head.\n'OW! What'd you do that for?'\n'I'm about to die for you, I'm allowed one hit.'\nBoris sighs and turns to you.\n'You must excuse my brother here, he's not the best with Russian. I told him to no one speaks Ukranian anymore but does he listen? No! Anyway, we're not trying to kill yourselves, just sacrifice our life for the other.'");
            Dialogue.Choice bAndGChoice5 = new Dialogue.Choice("'Heh, can you believe this guy Gleb? Man you must have missed a few Divine Liturgy's when you were younger. Naughty, naughty! As a good little Orthodox, you should know that we're called to sacrifice our lives to God or his children. It's our calling to be marytrs so that we may go up and meet the Big Guy. But will my dear brother let me go and realize my destiny? No! He keeps trying to hold me back so that he may sacrifice himself for MY cause, and that simply won't do!.'");
            Dialogue.Choice bAndGChoice6 = new Dialogue.Choice("'Because you can't sacrifice yourself for a dead person?'\nGleb chimed in.\n'Man you really are a dense one.'");
            Dialogue.Choice bAndGChoice7 = new Dialogue.Choice("'Yes, exactly! If you don't lay your life down for the greater good, then what's the point? And that means, despite my brother's rejection, I must go.'\nGleb has finished taking care of his wound and rises up.\n'And what will I be left to do? Walk around this world knowing I could've given my life for you but I didn't? No I can't do that.'\nHe turns to you, pleading.\n'Please you have to hold him back and let me go. It's my destiny.\n'Excuse you, it's not only MY destiny too but I thought of it first!' Boris interrupts. He turns to you.\n'No, you have to hold Gleb back and let ME go! I won't be able to live with myself if I'm the one left here.'\nYou don't know what to do, but it's clear they've given you the final call.");
            Dialogue.Choice bAndGChoice8 = new Dialogue.Choice("'Don't be silly, there is no middle ground here! We either are martyrs or we aren't. It's do or die, literally. Choose!'");
            Dialogue.Choice bAndGChoice9 = new Dialogue.Choice("Their apprehensiveness turns to shock as you turn to calmly walk towards the edge of the bridge. They try to stop you but you manage to climb over the rail in time, and drop.\n\nAfter a few seconds of freefall you realize this might not have been the smartest idea. That epiphany hits you as hard as the water will in one second more until you suddenly are suspended in the air, inches from the surface. A circle of shimmering light now holds you up, and as you get back on your feet, you realize your being held by a monk in black robes. He has a scruffy beard and doesn't look all too happy to see you.\n'You know you could just go to church right? It's a lot easier than this whole sacrifice thing.'\n\nYou're too amazed to even sputter out an answer, so he just sighs.\n'Hold on a second will you? ... This is Anatoli, checking in. Caught the martyr. Just gonna give him his blessing and put him back on the bridge.'\nAnd with that you're lifted up and placed back on the road. You try to get a look at your savior but the light dissapears as quickly as it showed up, and you're left only with two incredulous brothers.\n'Wuh...'\n'How?'\nYou can't even explain it yourself so you pay them no mind, but as you turn to leave, you realize you have something in your pockets. You pull out a piece of paper and unfold it, before nearly dropping it out of shock.\n\n'Immortality For a Day. Thanks for Your Sacrafice-The Big Guy'");
            Dialogue.Choice bAndGChoice10 = new Dialogue.Choice("You manage to race over to Gleb and hold him back. He begins to scream some very un Christian things at you while Boris simply smiles, steps over the guardrail, and jumps. Time seems to slow for a few seconds before a great light shoots down from the clouds and suddenly, Boris reappears, bathed in light and helped up by a strange man in black robes. He wears the clothes of a monk and has a scruffy beard. Gleb can only stare.\n'Father Anatoli himself...'\nAnd as quick as they appeared, the light goes out, and the two are gone. You let go and Gleb tumbles to the ground, in a state of near shock. You feel bad, but there isn't anything more you can do at this point. Maybe it's time to go.");
            Dialogue.Choice bAndGChoice11 = new Dialogue.Choice("You manage to race over to Boris and hold him back. He begins to scream some very un Christian things at you while Gleb simply smiles, steps over the guardrail, and jumps. Time seems to slow for a few seconds before a great light shoots down from the clouds and suddenly, Gleb reappears, bathed in light and helped up by a strange man in black robes. He wears the clothes of a monk and has a scruffy beard. Boris can only stare.\n'Father Anatoli himself...'\nAnd as quick as they appeared, the light goes out, and the two are gone. You let go and Boris tumbles to the ground, in a state of near shock. You feel bad, but there isn't anything more you can do at this point. Maybe it's time to go.");
            Dialogue.Choice bAndGChoice12 = new Dialogue.Choice("BIBLE ONE!");
            Dialogue.Choice bAndGChoice13 = new Dialogue.Choice("The two are still stupified. It doesn't look like they'll talk to you.");
            allChoices.AddRange( new List<Dialogue.Choice>() { bAndGChoice1, bAndGChoice2, bAndGChoice3, bAndGChoice4, bAndGChoice5, bAndGChoice6, bAndGChoice7, bAndGChoice8, bAndGChoice9, bAndGChoice10, bAndGChoice11, bAndGChoice12, bAndGChoice13 } );

            bAndG.charDialogue.Connect(bAndGChoice1, bAndGChoice2, "Excuse me?");
            bAndG.charDialogue.Connect(bAndGChoice2, bAndGChoice3, "What are you doing?");
            bAndG.charDialogue.Connect(bAndGChoice3, bAndGChoice4, "... What?");
            bAndG.charDialogue.Connect(bAndGChoice4, bAndGChoice5, "Why the hell would you want to do that?!");
            bAndG.charDialogue.Connect(bAndGChoice5, bAndGChoice6, "Why can't you both do it?");
            bAndG.charDialogue.Connect(bAndGChoice5, bAndGChoice7, "So... You're trying to sacrifice yourself for your brother, who wants to sacrifice himself for YOU, and  you can't agree on who gets to do it?");
            bAndG.charDialogue.Connect(bAndGChoice6, bAndGChoice7, "So... You're trying to sacrifice yourself for your brother, who wants to sacrifice himself for YOU, and  you can't agree on who gets to do it?");
            bAndG.charDialogue.Connect(bAndGChoice7, bAndGChoice8, "Haven't you guys every heard of nuance?");
            bAndG.charDialogue.Connect(bAndGChoice8, bAndGChoice9, "You know what, I'm sick of this. (Sacrifice Yourself)");

            bAndG.charDialogue.origHead = bAndGChoice1;

            theBridge.addCharacterToRoom(bAndG);

            //////////////////////
            /// THE THRESHOLD
            //////////////////////


            Room threshhold = new Room();
            threshhold.setUpRoom("The Threshold", "A rather shabby bar, situated on the fringe of town. Half of it lies destroyed, and bricks and roof tiles lie around it in piles. Nechaev stands above three Women who appear to be doing the actual work. You notice a sign a sign on the ground, torn from the broken door. It reads 'The Threshold-Bar and Grill'. You notice only 1 exit, to the South.");
            rooms.Add(threshhold);

            Character nechaev = new Character();
            List<String> nechaevNames  = new List<String> { "man", "nechaev", "the man" };
            nechaev.createChar(nechaevNames, 8);
            allChar.Add(nechaev);

            Character revWomen = new Character();
            List<String> revWomenNames  = new List<String> { "women", "working women", "woman", "rev women", "revolutionary women" };
            revWomen.createChar(revWomenNames, 9);
            allChar.Add(revWomen);


            Dialogue.Choice nechaevChoice1 = new Dialogue.Choice("The man finishes the shot he's holding before turning to you. He smiles, and before you can even react, envolps you in a massive bear hug.\n'Brother! Thank you so much for coming to our humble little band of comrades. We're honored to have you!'");
            Dialogue.Choice nechaevChoice2 = new Dialogue.Choice("He has an infectious energy that seems to light up the otherwise downtrodden room.\n'No, not personally, but we all are family in a spiritual sense. Togetherness is the only way we'll make change after all!'");
            Dialogue.Choice nechaevChoice3 = new Dialogue.Choice("'You could say that, although we've been reading the same book for 70 years. I think we're the first Russians to ever stick with something!'\nHe gives a hearty laugh");
            Dialogue.Choice nechaevChoice4 = new Dialogue.Choice("'See that's the beauty of it! We change so much yet nothings actually been done So, not only do we have to change the system, we must tear it all the way down! It's like doing a home makeover, except much for fun because we get to kill the rich people!'");
            Dialogue.Choice nechaevChoice5 = new Dialogue.Choice("'Oh yes, it's what we do best! You can't make an omelet without breaking a few eggs after all!'\nYou feel unsettled, but strangely trapped in his allure. ");
            Dialogue.Choice nechaevChoice6 = new Dialogue.Choice("'Oh it's not just a book, it's a way of life! The list of rules that must be followed to bring an end to rules! Oh how I love irony...'");
            Dialogue.Choice nechaevChoice7 = new Dialogue.Choice("'Why, isn't it obvious? Destroy it all! Bring this false state to it's knees and build a beautiful new world on the ashes of the old. It's almost too perfect of a plan, really. Everything reduced to its most bare state so it can be reborn....'");
            Dialogue.Choice nechaevChoice8 = new Dialogue.Choice("He becomes agitated. It's the first time you haven't seen him with a smile.\n'No, no, that's much too simplistic. We MUST break 'stuff'! We MUST destroy it all! To not do that is a betrayal. Don't you see what they're doing to my bar?'\nHe motions to the women toiling tiressly near the wall.\n'We rebuild the place every other week. Helps keep us centered in our mission.'");
            Dialogue.Choice nechaevChoice9 = new Dialogue.Choice("'Yes, and myself too. At least twice a week. We all do actually. You really must try it some time, it's like doing angry yoga, really gets you loose.'");
            Dialogue.Choice nechaevChoice10 = new Dialogue.Choice("'For the Revolution? Very. We're supposed to give our lives for it, to devote ourselves entirely to it, after. For my human body though? Not too much. Thank Lenin for asprin.'");
            Dialogue.Choice nechaevChoice11= new Dialogue.Choice("'Some of the greatest of our people! Revolutionaries from every era finally united as one! An unstoppable force made up entirely of the better gender. Although, of course they have to be the one doing this kind of work. They must prove their as equal as any man after all. Not that we discriminate of course.'");
            Dialogue.Choice nechaevChoice12 = new Dialogue.Choice("'Of course they are, we're all one big group!'\nOne of the women clears her throat as if to speak but only you seem to notice. The man continues to talk over her.\n'They love that they finally found a place where they're considered equal, where they're heard, where they're able to do a man's work alongside us. I'm a very progressive person you know...'");
            Dialogue.Choice nechaevChoice13 = new Dialogue.Choice("The soft spoken voice turns hard when you ask him that.\n'Because of those insolent royals and fat cats who would do anything in their power to keep US from that power. Those insolent bastards with their English whisky and French educations, bah! You might have met the worst one of them actually, old Alex II. He's the tsarpin around here and if we managed to put him into the dirt we'd win, but try as we might we can't get through to him. He's built some giant bunker on the fields where the serfs used to farm and shuts down every time we get within a 5 mile radius. So dissapointing... Mayakovsky has a poem written for the occasion and everything!'");
            Dialogue.Choice nechaevChoice14 = new Dialogue.Choice("His eyes widen considerably.\n'I don't know about voices, but I do have a pistol. In fact, a very important pistol. One which, if used right, could bring down an entire nation...'\nHe stops to ponder for a second before the smile once again.\n'You know, I think we have an opportunity here to help each other. As I said, we're being stonewalled by old Alex at every turn. We can't end his reign of terror, but since you're new about here, he won't suspect a thing. I will give you what you seek if you kill him for me. Prove you're my brother. Strike down the old and let the new rise up.'");
            Dialogue.Choice nechaevChoice15 = new Dialogue.Choice("He laughs. A short, sharp thing.\n'No there is no other way, this is binary! We either take over, or we don't, and that means he needs to be gone. And to prove that you've done it, I want you to bring back one of his medals. God I hated them growing up, so shiny and regal while we had nothing... Regardless. don't be afraid, just embrace your destiny. I can't wait to see you again.'\nAnd with that, he pats you on the back and goes back to deconstructing his bar.");

            Dialogue.Choice nechaevChoice16 = new Dialogue.Choice("He sees you approach and greets you warmly.\n'Ah brother, is the task completed yet?'");
            Dialogue.Choice nechaevChoice17 = new Dialogue.Choice("'Hmph, shame. I expected more from you I must say. Well, back to it!'\nAnd with that he turns away.");
            Dialogue.Choice nechaevChoice18 = new Dialogue.Choice("His eyes glow with energy as his trembling hands take the gift.\n'My, my, look at it, look at it! Our destiny, finally fufilled. You truly are a brother of the Revolution.'\nHe and the woman who have gathered at his side beam at you (although he's happier then they are).\n'Well, I think it's time you get your reward. Thank you for fulfilling your destiny, Iosef.'\n\nYou wonder why he called you that when your vision goes dark.\n\nAnd suddenly, you're awake.");

            allChoices.AddRange( new List<Dialogue.Choice>() { nechaevChoice1, nechaevChoice2, nechaevChoice3, nechaevChoice4, nechaevChoice5, nechaevChoice6, nechaevChoice7, nechaevChoice8, nechaevChoice9, nechaevChoice10, nechaevChoice11, nechaevChoice12, nechaevChoice13, nechaevChoice14, nechaevChoice15, nechaevChoice16, nechaevChoice17, nechaevChoice18 } );

            nechaev.charDialogue.Connect(nechaevChoice1, nechaevChoice2, "Do I know you?");
            nechaev.charDialogue.Connect(nechaevChoice2, nechaevChoice3, "So you guys like a book club or something?");
            nechaev.charDialogue.Connect(nechaevChoice3, nechaevChoice4, "I thought you said you wanted change though?");
            nechaev.charDialogue.Connect(nechaevChoice4, nechaevChoice5, "Kill the rich people?");
            nechaev.charDialogue.Connect(nechaevChoice3, nechaevChoice6, "It must be a good book then");
            nechaev.charDialogue.Connect(nechaevChoice6, nechaevChoice7, "So what exactly are you trying to do?");
            nechaev.charDialogue.Connect(nechaevChoice5, nechaevChoice7, "So what exactly are you trying to do?");
            nechaev.charDialogue.Connect(nechaevChoice7, nechaevChoice8, "So you just want to break stuff?");
            nechaev.charDialogue.Connect(nechaevChoice8, nechaevChoice9, "You destroy your own business?");
            nechaev.charDialogue.Connect(nechaevChoice9, nechaevChoice10, "Is that healthy?");
            nechaev.charDialogue.Connect(nechaevChoice10, nechaevChoice11, "Who are those women?");
            nechaev.charDialogue.Connect(nechaevChoice8, nechaevChoice11, "Who are those women?");
            nechaev.charDialogue.Connect(nechaevChoice11, nechaevChoice12, "And they're ok with this?");
            nechaev.charDialogue.Connect(nechaevChoice12, nechaevChoice13, "So why haven't you changed things then?");
            nechaev.charDialogue.Connect(nechaevChoice10, nechaevChoice13, "So why haven't you changed things then?");
            nechaev.charDialogue.Connect(nechaevChoice12, nechaevChoice9, "You destroy your own business?");
            nechaev.charDialogue.Connect(nechaevChoice13, nechaevChoice14, "You know, a voice told me to find the Revolutionaries Pistol. I think you might be the man I'm looking for");
            nechaev.charDialogue.Connect(nechaevChoice14, nechaevChoice15, "Wait, kill him? Is there another way?");

            nechaev.charDialogue.Connect(nechaevChoice16, nechaevChoice17, "No, not yet");

            
            nechaev.charDialogue.origHead = nechaevChoice1;

            threshhold.addCharacterToRoom(nechaev);
            threshhold.addCharacterToRoom(revWomen);




            //////////////////////////////////////////////////////////////////////////////////
            ///
            /// TSAR ROW: ALEX II, ENDING
            ///
            //////////////////////////////////////////////////////////////////////////////////

            //////////////////////
            /// THE BUNKER
            //////////////////////

            Room theBunker = new Room();
            theBunker.setUpRoom("The Bunker", "A large meadow with a steel structure right in the middle of it. It appears to be on an old Farm, but there's no one here to work it. The bunker slopes deep into the ground and appears to only have one exit, a solid iron door with a small flap in it. A sign in front of it reads 'REVOLUTIONARIES STAY OUT!-Alex'. You notice exits to the North and to the West.");
            rooms.Add(theBunker);

            Character alex = new Character();
            List<String> alexNames = new List<String> { "alex", "alexander", "bunker", "the bunker", "alexander II", "alex II" };
            alex.createChar(alexNames, 5);
            allChar.Add(alex);

            theBunker.addCharacterToRoom(alex);

            Item serfFarm = new Item();
            List<String> serfFarmNames = new List<String> { "Abandoned Farm", "farm", "the farm", "big farm", "old farm", "abandoned farm" };
            serfFarm.initItem(serfFarmNames, 10, "An abandoned field that looks like it once was a farm. You can see a rundown house in the corner that clearly hasn't been attended to in some time. It's as if the workers simply got up and left. How strange.");
            theBunker.addItemToRoom(serfFarm);

            Dialogue.Choice alexChoice1 = new Dialogue.Choice("Walking up to the bunker makes the door slam shut. A small flap opens and you can see two beady eyes peering out at you. A disembodied voice shouts out\n'Not one step closer asshole! Stay where you are!'");
            Dialogue.Choice alexChoice2 = new Dialogue.Choice("'Don't give me that act! Your friends already tried to knock me off before breakfast and I'm not having it again! You take one more step and I'll, I'll... do something drastic!'");
            Dialogue.Choice alexChoice3 = new Dialogue.Choice("The beady eyes look you over frantically until he realizes the truth.\n'Huh, you're right. I don't recognize you from all the attacks. Hm. Disarming landmines.\nYou hear a metallic click and the door swings open to reveal a balding, middle aged man with a bushy mustache in a wheelchair. He wears a bloodstained military uniform and, most disturbingly, is missing both of his legs.\n'I'm sorry about that, just been on edge a lot lately. How can I help you?'");
            Dialogue.Choice alexChoice4 = new Dialogue.Choice("He sighs.\n'Do people really forget this fast? I change the entire nation, free a whole people, fight a war, and yet...'\nHe trails off, lost in his own anger over the past, but then returns to the present.\n'You can just call me Alex.'");
            Dialogue.Choice alexChoice5 = new Dialogue.Choice("'Well, when you have a bunch of homicidal maniacs after your head all the time, you'd buy some defensive hardware too. This bunker only costs me a 1000 kopeks so the mines were pennys on the dollar.");
            Dialogue.Choice alexChoice6 = new Dialogue.Choice("He throws up his hands, exasperated.\n'I've been asking that question at least 5 times a day. Seriously, it makes no sense. I free everyone, make reforms, and what do I get? A nice dacha in the Crimea? Maybe a Sainthood? No! I get every socialist moron with a flimsy pistol trying to shoot my ass! How's that fair, huh?");
            Dialogue.Choice alexChoice7 = new Dialogue.Choice("'Do I know who's behind it, he asks. Of course I know who's behind it! They've shouted their names and slogans every time they try to blow me up! Which, by the way, is at least once a week. I wish they would at least do it on an organized schedule so I can plan around it but when do I ever get nice things... Anyway, It's that blasted Nechaev and his Merry Band of Idiots. They think I didn't go far enough in my reforms and now want my head. That's the last time I try to help people, that's for sure...");
            Dialogue.Choice alexChoice8 = new Dialogue.Choice("'Ah yes, that's from the time they actually got me. I dodge and weaved out of the way of every one of their gunmen, but sometimes you just can't outrun a bomb. That's why I got this bunker actually. Too risky to go outside nowadays.");
            Dialogue.Choice alexChoice9 = new Dialogue.Choice("'Hah. Unless you know a way to kill that blasted pig or give me immortality, I don't think so. This might be a game of cat and mouse that plays out for eternity. Until you figure those out though, I'm afraid I must beg my leave. I've been exposed for too long and God only knows what those nutjobs will try next. Good day'.\nAnd with that, the door shuts with a loud clang. You can still hear him ranting about 'those ungrateful serfs' as he shuts the flap.");

            Dialogue.Choice alexChoice10 = new Dialogue.Choice("Walking up to the bunker makes the door slam shut. A small flap opens and you can see two beady eyes peering out at you.\n'Ah, the sane one. How goes your search for immortality?'");
            Dialogue.Choice alexChoice11 = new Dialogue.Choice("'Wait, what? Are you serious? Hold on a second.'\nThe door creaks as he fumbles with the locks\n'Blasted f***ing thing, I'm getting you replaced with electronics when this is over.'\nHe finally gets it unstuck and moves into the light.\n'Alright, let's see it.'");
            Dialogue.Choice alexChoice12 = new Dialogue.Choice("'Ha. Told you. Why must no one listen to me?' He sighs, and the flap slams shut.");
            Dialogue.Choice alexChoice13 = new Dialogue.Choice("The words don't even reach his ears before the great man falls to the ground, dead. You're shake as you walk over to him and clutch one of the medals that lie on his chest. You feel like you should be feeling something, regret, remorse, anger, fear, but the most frightening thing at all is that there really isn't much there. You were comfortable doing this in the name of a cause.\n\nThe medal comes loose with one sharp pull. It's time to go back to Nechaev.");
            Dialogue.Choice alexChoice14 = new Dialogue.Choice("The words don't even reach his ears before the great man falls to the ground, dead. You're shake as you walk over to him and clutch one of the medals that lie on his chest. You feel like you should be feeling something, regret, remorse, anger, fear, but the most frightening thing at all is that there really isn't much there. You were comfortable doing this in the name of a cause.\n\nThe medal comes loose with one sharp pull. It is time to go back to Nechaev");
            Dialogue.Choice alexChoice15 = new Dialogue.Choice("'These guys were trying to martyr themselves and I ended up getting a blessing from God. I think it's a free pass on immortality for a day, and I'd like you to have it.'\nThe old man is nervous at first, but at your beckoning comes over and examines the paper. His fear turns to surpise, and suprise to happiness. He chuckles.\n'Wow, you really did pull it off. There is still good in your heart after all.'\nHe takes your hand.\n'Thank you Iosef.'\n\nYou wonder why he called you that when your vision goes dark.\n\nAnd suddenly, you're awake.");
            Dialogue.Choice alexChoice16 = new Dialogue.Choice("He's dead.\nYou can't talk to dead people.");

            allChoices.AddRange( new List<Dialogue.Choice>() { alexChoice1, alexChoice2, alexChoice3, alexChoice4, alexChoice5, alexChoice6, alexChoice7, alexChoice8, alexChoice9, alexChoice10, alexChoice11, alexChoice12, alexChoice13, alexChoice14, alexChoice15, alexChoice16 } );

            alex.charDialogue.Connect(alexChoice1, alexChoice2, "Excuse me?");
            alex.charDialogue.Connect(alexChoice2, alexChoice3, "Hey, hey, it's ok, I'm not one of those revolutionaries!");
            alex.charDialogue.Connect(alexChoice3, alexChoice4, "Who are you?");
            alex.charDialogue.Connect(alexChoice3, alexChoice5, "Uh, landmines??");
            alex.charDialogue.Connect(alexChoice4, alexChoice6, "Why is someone trying to kill you?");
            alex.charDialogue.Connect(alexChoice5, alexChoice6, "Why is someone trying to kill you?");
            alex.charDialogue.Connect(alexChoice6, alexChoice7, "Do you know who's behind it?");
            alex.charDialogue.Connect(alexChoice7, alexChoice8, "If you don't mind me asking, what happened to your legs?");
            alex.charDialogue.Connect(alexChoice7, alexChoice9, "Is there anything I can do to help?");
            alex.charDialogue.Connect(alexChoice8, alexChoice9, "Is there anything I can do to help?");

            alex.charDialogue.Connect(alexChoice10, alexChoice12, "I haven't found it yet.");


            alex.charDialogue.origHead = alexChoice1;


            //////////////////////////////////
            /// ADDING EXITS
            /////////////////////////////////

            redSquare.addExit(new Exit(Exit.Directions.North, multiverse)); /// Need to do this down here since all the rooms need to be declared first
            multiverse.addExit(new Exit(Exit.Directions.North, terminus));
            terminus.addExit(new Exit(Exit.Directions.East, bulgakovLane));
            terminus.addExit(new Exit(Exit.Directions.West, kalabukhovHouse));
            terminus.addExit(new Exit(Exit.Directions.North, tatlinSquare));
            bulgakovLane.addExit(new Exit(Exit.Directions.West, terminus));
            kalabukhovHouse.addExit(new Exit(Exit.Directions.East, terminus));
            kalabukhovHouse.addExit(new Exit(Exit.Directions.North, theBlock));
            theBlock.addExit(new Exit(Exit.Directions.South, kalabukhovHouse));
            tatlinSquare.addExit(new Exit(Exit.Directions.South, terminus));
            tatlinSquare.addExit(new Exit(Exit.Directions.East, theBunker));
            tatlinSquare.addExit(new Exit(Exit.Directions.North, theBridge));
            theBridge.addExit(new Exit(Exit.Directions.South, tatlinSquare));
            theBunker.addExit(new Exit(Exit.Directions.West, tatlinSquare));
            theBridge.addExit(new Exit(Exit.Directions.North, threshhold));
            threshhold.addExit(new Exit(Exit.Directions.South, theBridge));

            LoadCurrentRoom(redSquare); /// You start here, so it needs to load it in. Don't do this for the rest.


        }

        public void Update()
		{
            if(!inDialogue)
            {
                Console.Write("\n\nPlease Enter a Command: ");
                string currentCommand = Console.ReadLine().ToLower(); /// This is what reads what you type in the console. It's parsed into DoAction if you haven't quit the game

			    // instantly check for a quit
			    if (currentCommand == "quit" || currentCommand == "q")
			    {
				    isRunning = false;
				    return;
			    }

                if (!gameOver)
                {
                    // otherwise, process commands.
                    doAction(currentCommand);
                }

                else
                {
                    Console.WriteLine("\nNope. Time to quit.\n");
                }
            }			
		}

        private void getCurrentCordDebug() /// If you want a quick reference to cordinates, just call this
        {
            Console.WriteLine("Current Pos: (" + cordinates[0].ToString() + ", " + cordinates[1].ToString() + ")");
        }

        private void LoadCurrentRoom(Room r) /// Loads in the current room. Writes the title, desc, etc
        {
            currentRoom = r; /// Sets the current room to the new one
            Console.WriteLine(r.roomTitle + "\n\n" + r.roomDesc); /// Writes out the title and desc
        }

        public static void GameOver()
        {
            Console.WriteLine("GAME OVER. YOU NEED TO RESTART NOW. THANKS FOR PLAYING!!!");
            initGame.Game.isRunning = false;
        }


        private bool moveRoom(string command)
        {
            foreach ( Exit exit in currentRoom.getExits() )
            {
                if (command == exit.ToString().ToLower() || command == exit.getShortDirection().ToLower())
                {
                    Room newRoom = exit.getLeadsTo();
                    Console.WriteLine("\nYou move " + exit.ToString() + " to:\n");
                    LoadCurrentRoom(newRoom);
                    return true;
                }
            }
            return false;
        }

        private void doAction(string a) /// Where the magic happens
        {
            

            ////////////////////////////////////////////////////////////////
            /// MOVEMENT 
            ////////////////////////////////////////////////////////////////


            if(directions.Contains(a)) /// If it's a direction, since this implies moving between rooms and shit
            {
                moveRoom(a);
            }

            ////////////////////////////////////////////////////////////////
            /// TALKING/INTERACTING WITH CHARACTERS
            ////////////////////////////////////////////////////////////////

            else if(a.Contains("talk") || a.Contains("talk to") || a.Contains("speak") || a.Contains("speak to") || a.Contains("approach") || a.Contains("interact with") || a.Contains("pet")) /// If this is a command to talk to someone else
            {
                if(currentRoom.roomChar != null)
                {
                    bool foundSomeone = false;
                    foreach(Character c in currentRoom.roomChar)
                    {
                        foreach(string s in c.charNameList)
                        {
                            if(a.Contains(s) && c.canTalk == true)
                            {
                                inDialogue = true;
                                c.charDialogue.head = c.charDialogue.origHead;
                                c.charDialogue.loadDialogue();

                                inDialogue = false;
                                foundSomeone = true;
                                break;
                            }
                        }
                    }

                    if(foundSomeone == false)
                    {
                        Console.WriteLine("There's no one here to talk to with that name");
                    }
                }
                else
                {
                    Console.WriteLine("There's no one to talk to here");
                }
            }

            ////////////////////////////////////////////////////////////////
            /// INSPECTING ITEMS
            ////////////////////////////////////////////////////////////////

            else if(a.Contains("look at") || a.Contains("inspect") || a.Contains("look"))
            {
                bool foundItem = false;

                foreach(Item i in currentRoom.roomItems)
                {
                    foreach(string s in i.itemNameList)
                    {
                        if(a.Contains(s))
                        {
                            Console.WriteLine(i.itemDesc);
                            foundItem = true;
                            break;
                        }
                    }
                }

                foreach(Item i in player.inventory)
                {
                    foreach(string s in i.itemNameList)
                    {
                        if(a.Contains(s))
                        {
                            Console.Write(i.itemDesc);
                            foundItem = true;
                            break;
                        }
                    }
                }

                if(foundItem == false)
                {
                    Console.WriteLine("\nThere's no item with that name.");
                }
            }

            ////////////////////////////////////////////////////////////////
            /// DISPLAY ITEMS
            ////////////////////////////////////////////////////////////////

            else if(a.Contains("inventory"))
            {

                if(player.inventory.Count != 0)
                {
                    Console.WriteLine("You currently have:");

                    foreach(Item i in player.inventory)
                    {
                        Console.WriteLine("\n" + i.itemNameList[0]);
                    }
                }

                else
                {
                    Console.WriteLine("\nYou don't have anything in your inventory yet.");
                }
            }


            ////////////////////////////////////////////////////////////////
            /// USE ITEMS
            ////////////////////////////////////////////////////////////////

            else if(a.Contains("use"))
            {
                int itemBeingUsedID = 0; /// For some reason it won't just let me keep this as null, it makes me assign a value. Weird

                foreach(Item i in player.inventory)
                {
                    foreach(string s in i.itemNameList)
                    {
                        if(a.Contains(s))
                        {
                            itemBeingUsedID = i.itemID;
                        }  
                    }
                }

                if(itemBeingUsedID == 2)
                {
                    Console.WriteLine("Fuck you bitch");
                }
            }
            

            ////////////////////////////////////////////////////////////////
            /// HELP
            ////////////////////////////////////////////////////////////////

            else if(a.Contains("help") || a.Contains("commands"))
            {
                Console.WriteLine("------------------------------------------------------------------------------------------------------------------------------------------------\nHello and welcome to One Night in Russia! The commands you need to play this game are as follows:\n\nDIRECTIONS:\nN or North to go up\nS or South to go down\nW or West to go left\nE or East to go right\n\nINTERACTING:\nTalk, Talk to, or Speak along with a characters name to talk to them\nLook at, Inspect, or look along with an items name to look at it\n\nUSING ITEMS:\nUse along with an items name to use it\nInventory to display your current inventory\n\nOTHER:\nDesc or Description to display the current room's description\nUse Help or Commands to list out all available commands");
            }

            else if(a.Contains("description") || a.Contains("desc"))
            {
                Console.WriteLine(currentRoom.roomDesc);
            }

            else
            {
                Console.WriteLine("That's not a reconized command. Please try again, or type 'help' for all your options");
            }
        }
    }
}