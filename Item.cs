using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class Item
    {
        public List<string> itemNameList = new List<string>(); /// Index 0 is reserved for the proper name of the item. 1-inf are the callable names

        public int itemID;

        public string itemDesc;

        public bool canPickUp = false;

        public void initItem(List<string> l, int i, string d) 
        {
            itemNameList = l;
            itemID = i;
            itemDesc = d;
        }

        public void enablePickup()
        {
            canPickUp = true;
        }
    }
}