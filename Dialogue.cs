using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure
{
    class Dialogue
    {


        public Choice origHead;

        public Choice head;

        public void loadDialogue()
        {
            while (true)
            {
                Console.WriteLine($"\n{head.answer}");
                int count = 1;

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                /// BLOCK FOR ADDING IN DIALOGUE CONSEQUENCES, Comments used as an example for how to do it
                ////////////////////////////////////////////////////////////////////////////////////////////


                ////////////////////////////
                /// VLAD GIVING THE BIBLE
                ////////////////////////////

                ///head.answer == the text of the choice that's supposed to do something.

                if(head.answer == "He seems confused, but the scowl slowly turns to a smile\n'You know, I think that truly was the right answer.'\nHe turns back to look at the Church behind him, savoring it's beauty.\n'I picked our religion for its joy, the pageantry, the service that seemed to create heaven on Earth. We can't quantify our love for God in how many saints we've memorized, but I think our Western brothers would disagree. But what do they know, they never had to jump into a river...'\nHe gets lost in his memories before reaching for his book, thrusting it out before him.\n'Here, take it. I think you've earned it.'\nAnd with that, he slips another one out from under his robes and continues reading.\n\nYou realize he had been covering up an inscription on the first page\n\n'To Vladimir, from your grandmother. Love, Olga'")
                {

                    ////////////////////////////////////////
                    /// Creates the Bible Item
                    ////////////////////////////////////////

                    Item vladBible = new Item();
                    List<String> vladBibleNames = new List<String> { "Vladimir's Bible", "vlads bible", "bible", "the bible"};
                    vladBible.initItem(vladBibleNames, 1, "A worn out bible, clearly used. It has an inscription on the first page:\n'To Vladimir, from your grandmother. Love, Olga'");
                    Game.player.inventory.Add(vladBible);

                    /// Creates a null character placeholder and null choice placeholders for all the ones that need to have a connection severed, as well as the thing being decoupled.
                    Character c = null;
                    Choice choice1 = null;

                    Character b = null; /// Boris and Gleb
                    Choice b1 = null; /// Boris choice 1
                    Choice b2 = null; /// Boris choice 2


                    foreach(Character i in Game.allChar) /// Gets the character we're trying to work with from the list of all characters
                    {
                        if(i.charID == 1)
                        { 
                            c = i;
                        }

                        else if(i.charID == 7)
                        {
                            b = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "He doesn't even look up from his book. You don't think you will speak again. You feel sad.")
                        {
                            choice1 = i;
                        }

                        else if(i.answer == "'Don't be silly, there is no middle ground here! We either are martyrs or we aren't. It's do or die, literally. Choose!'")
                        {
                            b1 = i;
                        }

                        else if(i.answer == "BIBLE ONE!")
                        {
                            b2 = i;
                        }
                    }

                    /// The Actual decoupling 

                    c.charDialogue.origHead = choice1;
                    b.charDialogue.Connect(b1, b2, "Wait! I have a blessed Bible, could that give you what you want?");
                }


                ////////////////////////////////////////////////////
                /// PROF GIVING YOU SAUSAGE, FAKING AS HOUSING COM
                ////////////////////////////////////////////////////

                ///head.answer == the text of the choice that's supposed to do something.

                else if(head.answer == "'Ugh, just when I thought I had managed to escape your grasp.'\nHe shakes his head and opens the package he has been holding. It's full of sausage.\n'Here, take one of these and never bother me again, or I'll sick the central commitee onto you! Now leave!'\n\nHe angrily shoves the gift into your hands and pulls down the brim of his hat, once again lost in his own thoughts. As you turn to leave, you hear him softly sing.\n\n'From Seville to Granada...'" || head.answer == "'Well, now we're talking. Thank you for conducting buisness like a civilized gentleman.'\nHe breaks you off a piece off sausage, takes your money, and once again becomes lost in his own world.\n'Towards the sacred banks of the Nile...'")
                {
                    Item sausage = new Item();
                    List<String> sausNames = new List<String> { "Krakow Sausage", "sausage", "krakow sausage" };
                    sausage.initItem(sausNames, 2, "A piece of Krakow sausage. Fit for humans or a dog");
                    Game.player.inventory.Add(sausage);

                    Character c = null;
                    Character c2 = null; /// Sharik
                    Choice choice1 = null; ///The new begining choice

                    Choice choice2 = null; ///Giving Sausage to Sharik
                    Choice choice3 = null; ///Thing before choice 2

                    foreach(Item i in Game.player.inventory)
                    {
                        if(i.itemID == 5)
                        {
                            Game.player.inventory.Remove(i);
                            break;
                        }
                    }

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 2)
                        { 
                            c = i;
                        }

                        else if(i.charID == 3)
                        {
                            c2 = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "He pays you no attention, lost in his world. It's clear he's not going to talk to you. As you turn away, you hear him softly hum.\n\n'Towards the sacred banks of the Nile...")
                        {
                            choice1 = i;
                        }

                        else if(i.answer == "He is clearly grateful and lets out some happy barks before taking the sausage and running off. You're about to leave before you notice him pawing and scraping at the pile of dirt in front of the House. He pulls out a worn and dirty letter before running back and, to your amazement, standing up on his hind legs to give it to you. You take it despite your confusion and the dog walks away before plopping back down on all 4s. Slightly unsettled, you wipe the slobber off of the letter and turn it over. It reads 'From Kolya, Camp 12'")
                        {
                            choice2 = i;
                        }

                        else if(i.answer == "He gives a quick help before panting repeadetly. It doesn't look like he's eaten for some time.")
                        {
                            choice3 = i;
                        }
                    } 

                    c.charDialogue.origHead = choice1;

                    c2.charDialogue.Connect(choice3, choice2, "Here you go boy, I've got some sausage!");
                }

                ////////////////////////////////////////////////////
                /// REACH END OF PROF DIALOGUE
                ////////////////////////////////////////////////////

                else if(head.answer == "'Hmph, no. Not for any particular reason, mind you, I just don't want to. Despite what the government might want, I do still have some free will. Come back when you can pay.'")
                {
                    Character p = null; /// Prof
                    Dialogue.Choice c = null;

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 2)
                        { 
                            p = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "'Ah my beautiful friend has returned! Do you have the money?")
                        {
                            c = i;
                        }
                    }

                    p.charDialogue.origHead = c;


                }

                //////////////////////
                /// PROF - GAME OVER
                //////////////////////

                else if(head.answer == "He slowly looks you up and down.\n'Hm, I am feeling generous. Follow me, please, and I'm sure we can work something out.\nYou turn to follow his outstretched hand when a cloth is clamped over you mouth. You feel dizzy and before you know it, you've hit the ground..\n\nWhat comes next is too blurry to focus on, but you do remember a few things: the sharpness of the scalpel, the stench of anesthesia, the sterile coldness of the room. And above it all, that haunting melody, the last thing you'll here.\n\n'Towards the sacred banks of the Nile...'")
                {
                    Game.GameOver();
                }

                else if(head.answer == "'Ah wonderful, I'm so happy you are willing to help advance science. Please, just come this way.'\nYou turn to follow his outstretched hand when a cloth is clamped over your mouth. You try to struggle, but you feel weak and slump to the ground...\n\nWhat comes next is too blurry to focus on, but you do remember a few distint things: the sharpness of the scapel, the blinding lights, the sterile coldness of the room. And floating above it all, that haunting melody and the last thing you'll hear:\n\n'Towards the sacred banks of the Nile...'")
                {
                    Game.GameOver(); 
                }


                
                ////////////////////////////////////////////////////
                /// SHARIK GIVES YOU LETTER
                ////////////////////////////////////////////////////

                else if(head.answer == "He is clearly grateful and lets out some happy barks before taking the sausage and running off. You're about to leave before you notice him pawing and scraping at the pile of dirt in front of the House. He pulls out a worn and dirty letter before running back and, to your amazement, standing up on his hind legs to give it to you. You take it despite your confusion and the dog walks away before plopping back down on all 4s. Slightly unsettled, you wipe the slobber off of the letter and turn it over. It reads 'From Kolya, Camp 12'")
                {
                    Item letter = new Item();
                    List<String> letterNames = new List<String> { "Kolya's Letter", "letter", "kolya's letter", "kolyas letter" };
                    letter.initItem(letterNames, 3, "A dirty letter, given to you by a dog because that's apparently normal here.");
                    Game.player.inventory.Add(letter);

                    Character s = null; /// Sofia
                    Choice choice1 = null;
                    Choice choice2 = null;
                    Choice choice3 = null;
                    Choice choice4 = null;

                    Character c = null; //sharik
                    Choice cChoice1 = null;

                    foreach(Item i in Game.player.inventory)
                    {
                        if(i.itemID == 2)
                        {
                            Game.player.inventory.Remove(i);
                            break;
                        }
                    }

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 4)
                        { 
                            s = i;
                        }

                        if(i.charID == 3)
                        {
                            c = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "'No, no, NO! That's what I've been trying to tell them! My Kolya couldn't have betrayed Him, the thought never would have crossed his mind! He loved His system as much as he loved me. My son has been caught up in some grand mistake, that is all. I just need to write Him again and He'll fix it all, just you watch'")
                        {
                            choice1 = i;
                        }

                        else if(i.answer == "The chattering stops and the world seems to slow to a halt for her. She gingerly lifts her head and looks you right in the eyes. For the first time, you see something within them: Hope.\n'Yes, he is. How did you know?'")
                        {
                            choice2 = i;
                        }

                        else if(i.answer == "'Have you managed to find them yet, by any chance?'\nShe waits on pins and needles for your answer.")
                        {
                            choice3 = i;
                        }

                        else if(i.answer == "And just like that, she explodes into a ball of energy.\n'Oh, oh thank you, thank you so much! He did read my letters after all! Praise the System, Praise Him! May I please see it? I'm dying to know how my darling boy is.'")
                        {
                            choice4 = i;
                        }

                        else if(i.answer == "Walking up to the dog just makes him lazily roll over and stick out his tongue, expecting more meat. You go to give him a pat on the belly and move on. You notice he has a large scar there.")
                        {
                            cChoice1 = i;
                        }
                    }

                    s.charDialogue.Connect(choice1, choice2, "Is he in a place called Camp 12 by any chance?");
                    s.charDialogue.Connect(choice3, choice4, "Yes I did");

                    c.charDialogue.origHead = cChoice1;
                }

                ////////////////////////////////////////////////////////////////
                /// REORDER SOFIA DIALOGUE/ YOU PISS HER OFF AND GAME OVER
                ////////////////////////////////////////////////////////////////

                else if(head.answer == "'Oh thank you, thank you! May God bless you!'\nShe gives you a quick hug before once again returning to her apartment, lost once again in her blissful fantasy.")
                {

                    Character s = null; /// Sofia
                    Choice choice1 = null;

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 4)
                        { 
                            s = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "'Have you managed to find them yet, by any chance?'\nShe waits on pins and needles for your answer.")
                        {
                            choice1 = i;
                        }
                    }

                    s.charDialogue.origHead = choice1;
                }

                else if(head.answer == "'Oh thank you, thank you! May God bless you!'\nShe gives you a quick hug before once again returning to her apartment, lost once again in her blissful fantasy.")
                {

                    Character s = null; /// Sofia
                    Choice choice1 = null;

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 4)
                        { 
                            s = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "As you approach her, she notices and just adjusts her walking to avoid you. She won't even meet your eye. It's clear she won't speak to you anymore.")
                        {
                            choice1 = i;
                        }
                    }

                    s.charDialogue.origHead = choice1;
                }


                else if(head.answer == "The smile stays frozen on her face as she processes what you have just said. It slowly works its way through her stress addled brain: No getting out of this, no pleading to Him, no seeing her son again. And all of the pain inflected on her boils up and out.\n'no, no, No, No, NO!'\nShe shrieks right in your face before running off. It doesn't look like this was the right choice. You turn to leave and begin walking away before you're suddenly on the ground. You're head throbs and you spy the blood strained brick that was just thrown at your head. You try to get back up by your eyesight rapidly begins to falter, and the last thing you see is that poor, battered women pick up her letter and flee.\n\nEveryone has their limit.")
                {
                    Game.GameOver();
                }

                else if(head.answer == "'Hah. Unless you know a way to kill that blasted pig or give me immortality, I don't think so. This might be a game of cat and mouse that plays out for eternity. Until you figure those out though, I'm afraid I must beg my leave. I've been exposed for too long and God only knows what those nutjobs will try next. Good day'.\nAnd with that, the door shuts with a loud clang. You can still hear him ranting about 'those ungrateful serfs' as he shuts the flap.")
                {

                    Character a = null; /// Sofia
                    Choice choice1 = null;

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 5)
                        { 
                            a = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "Walking up to the bunker makes the door slam shut. A small flap opens and you can see two beady eyes peering out at you.\n'Ah, the sane one. How goes your search for immortality?'")
                        {
                            choice1 = i;
                        }
                    }

                    a.charDialogue.origHead = choice1;

                }

                ////////////////////////////////////////////////////////////////
                /// EISENSTEIN GIVES YOU THE MONEY
                ////////////////////////////////////////////////////////////////

                else if(head.answer == "'Oh, perfect, yes perfect! I just wanted to call you over to tell you myself. The little extra who came in late stealing the show. Only in cinema could that happen! Although, if the joy of creating art isn't the reward you want, I think this might suffice.'\n\nHe calls over an assistant and gives you a few kopeks.\n'Here's your pay for the day, my assistant will show you to the exit.'" || head.answer == "He laughs with a jolly tone.\n'Come on, cheer up. You're now going to be preserved for all of history, all on this little reel right here. Making cinema is supposed to be a happy thing, even when you're tired. Although, if the joy of creating art isn't your thing, I think this might suffice.'\n\nHe calls over an assistant and gives you a few kopeks.\n'Here's your pay for the day, my assistant will show you to the exit.'")
                {

                    Item kopeks = new Item();
                    List<String> kopecsNames = new List<String> { "5 Kopeks", "kopeks", "5 kopeks", "money", "the money" };
                    kopeks.initItem(kopecsNames, 5, "A dirty letter, given to you by a dog because that's apparently normal here.");
                    Game.player.inventory.Add(kopeks);

                    Character e = null; ///Eisenstein
                    Character p = null; ///Prof

                    Choice pChoice1 = null;
                    Choice pChoice2 = null;
                    Choice pChoice3 = null;
                    Choice eChoice = null;

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 2)
                        { 
                            p = i;
                        }

                        else if(i.charID == 6)
                        {
                            e = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "'Ah my beautiful friend has returned! Do you have the money?")
                        {
                            pChoice1 = i;
                        }

                        else if(i.answer == "'Well, now we're talking. Thank you for conducting buisness like a civilized gentleman.'\nHe breaks you off a piece off sausage, takes your money, and once again becomes lost in his own world.\n'Towards the sacred banks of the Nile...'")
                        {
                            pChoice2 = i;
                        }

                        else if(i.answer == "He lets out a sharp laugh.\n'Do I look like I'm stupid. I am not like those zealots on the Central Commitee. No, no, you will get nothing for free, I've got to pay for a 7 room apartment somehow. However, if you have a few kopeks, I think I can let a piece or two go.")
                        {
                            pChoice3 = i;
                        }

                        else if(i.answer == "He doesn't even look up from his camera. It looks like he's too busy to talk.")
                        {
                            eChoice = i;
                        }
                    }

                    p.charDialogue.Connect(pChoice1, pChoice2, "I have some kopecs right here.");
                    p.charDialogue.Connect(pChoice3, pChoice2, "I have some kopecs right here.");

                    e.charDialogue.origHead = eChoice;


                }

                ////////////////////////////////////////////////////////////////
                /// BORIS AND GLEB SHIT
                ////////////////////////////////////////////////////////////////

                else if(head.answer == "BIBLE ONE!" || head.answer == "Their apprehensiveness turns to shock as you turn to calmly walk towards the edge of the bridge. They try to stop you but you manage to climb over the rail in time, and drop.\n\nAfter a few seconds of freefall you realize this might not have been the smartest idea. That epiphany hits you as hard as the water will in one second more until you suddenly are suspended in the air, inches from the surface. A circle of shimmering light now holds you up, and as you get back on your feet, you realize your being held by a monk in black robes. He has a scruffy beard and doesn't look all too happy to see you.\n'You know you could just go to church right? It's a lot easier than this whole sacrifice thing.'\n\nYou're too amazed to even sputter out an answer, so he just sighs.\n'Hold on a second will you? ... This is Anatoli, checking in. Caught the martyr. Just gonna give him his blessing and put him back on the bridge.'\nAnd with that you're lifted up and placed back on the road. You try to get a look at your savior but the light dissapears as quickly as it showed up, and you're left only with two incredulous brothers.\n'Wuh...'\n'How?'\nYou can't even explain it yourself so you pay them no mind, but as you turn to leave, you realize you have something in your pockets. You pull out a piece of paper and unfold it, before nearly dropping it out of shock.\n\n'Immortality For a Day. Thanks for Your Sacrafice-The Big Guy'")
                {

                    Item blessing = new Item();
                    List<String> blessingNames = new List<String> { "God's Blessing", "blessing", "immortality", "the immortality", "gods note", "god's note" };
                    blessing.initItem(blessingNames, 6, "A dirty letter, given to you by a dog because that's apparently normal here.");
                    Game.player.inventory.Add(blessing);

                    foreach(Item i in Game.player.inventory)
                    {
                        if(i.itemID == 1)
                        {
                            Game.player.inventory.Remove(i);
                            break;
                        }
                    }

                    Character b = null;
                    Choice choice1 = null;

                    Character a = null;
                    Choice choicea1 = null;
                    Choice choicea2 = null;
                    Choice choicea3 = null;
                    Choice choicea4 = null;

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 7)
                        { 
                            b = i;
                        }

                        if(i.charID == 5)
                        {
                            a = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "The two are still stupified. It doesn't look like they'll talk to you.")
                        {
                            choice1 = i;
                        }

                        else if(i.answer == "'These guys were trying to martyr themselves and I ended up getting a blessing from God. I think it's a free pass on immortality for a day, and I'd like you to have it.'\nThe old man is nervous at first, but at your beckoning comes over and examines the paper. His fear turns to surpise, and suprise to happiness. He chuckles.\n'Wow, you really did pull it off. There is still good in your heart after all.'\nHe takes your hand.\n'Thank you Iosef.'\n\nYou wonder why he called you that when your vision goes dark.\n\nAnd suddenly, you're awake.")
                        {  
                            choicea1 = i;
                        }

                        else if(i.answer == "'Hah. Unless you know a way to kill that blasted pig or give me immortality, I don't think so. This might be a game of cat and mouse that plays out for eternity. Until you figure those out though, I'm afraid I must beg my leave. I've been exposed for too long and God only knows what those nutjobs will try next. Good day'.\nAnd with that, the door shuts with a loud clang. You can still hear him ranting about 'those ungrateful serfs' as he shuts the flap.")
                        {
                            choicea2 = i;
                           
                        }
                        
                        else if(i.answer == "'Wait, what? Are you serious? Hold on a second.'\nThe door creaks as he fumbles with the locks\n'Blasted f***ing thing, I'm getting you replaced with electronics when this is over.'\nHe finally gets it unstuck and moves into the light.\n'Alright, let's see it.'")
                        {
                            choicea3 = i;
                        }

                        else if(i.answer == "Walking up to the bunker makes the door slam shut. A small flap opens and you can see two beady eyes peering out at you.\n'Ah, the sane one. How goes your search for immortality?'")
                        {
                            choicea4 = i;
                        }



                    }

                    a.charDialogue.Connect(choicea2, choicea1, "Wait, I have that! Come back out!");
                    a.charDialogue.Connect(choicea3, choicea1, "This is the Tome of Martyrdom. It'll protect you from all those that want to do you harm. Have it. It's yours.");
                    a.charDialogue.Connect(choicea4, choicea3, "Yes I have!");

                    b.charDialogue.origHead = choice1;
                    
                }
            
                else if(head.answer == "He laughs. A short, sharp thing.\n'No there is no other way, this is binary! We either take over, or we don't, and that means he needs to be gone. And to prove that you've done it, I want you to bring back one of his medals. God I hated them growing up, so shiny and regal while we had nothing... Regardless. don't be afraid, just embrace your destiny. I can't wait to see you again.'\nAnd with that, he pats you on the back and goes back to deconstructing his bar.")
                {

                    Character n = null;
                    Choice choice1 = null;

                    Character a = null;
                    Choice choicea1 = null;
                    Choice choicea2 = null;
                    Choice choicea3 = null;
                    Choice choicea4 = null;

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 8)
                        { 
                            n = i;
                        }

                        if(i.charID == 5)
                        {
                            a = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "He sees you approach and greets you warmly.\n'Ah brother, is the task completed yet?'")
                        {
                            choice1 = i;
                        }

                        else if(i.answer == "The words don't even reach his ears before the great man falls to the ground, dead. You're shake as you walk over to him and clutch one of the medals that lie on his chest. You feel like you should be feeling something, regret, remorse, anger, fear, but the most frightening thing at all is that there really isn't much there. You were comfortable doing this in the name of a cause.\n\nThe medal comes loose with one sharp pull. It's time to go back to Nechaev.")
                        {  
                            choicea1 = i;
                        }

                        else if(i.answer == "The words don't even reach his ears before the great man falls to the ground, dead. You're shake as you walk over to him and clutch one of the medals that lie on his chest. You feel like you should be feeling something, regret, remorse, anger, fear, but the most frightening thing at all is that there really isn't much there. You were comfortable doing this in the name of a cause.\n\nThe medal comes loose with one sharp pull. It is time to go back to Nechaev")
                        {
                            choicea2 = i;
                           
                        }
                        
                        else if(i.answer == "'Wait, what? Are you serious? Hold on a second.'\nThe door creaks as he fumbles with the locks\n'Blasted f***ing thing, I'm getting you replaced with electronics when this is over.'\nHe finally gets it unstuck and moves into the light.\n'Alright, let's see it.'")
                        {
                            choicea3 = i;
                        }

                        else if(i.answer == "Walking up to the bunker makes the door slam shut. A small flap opens and you can see two beady eyes peering out at you.\n'Ah, the sane one. How goes your search for immortality?'")
                        {
                            choicea4 = i;
                        }

                    }

                    a.charDialogue.Connect(choicea3, choicea1, "I'm Sorry. (Shoot Him).");
                    a.charDialogue.Connect(choicea3, choicea2, "For Russia! (Shoot Him)");
                    a.charDialogue.Connect(choicea4, choicea3, "Yes I have!");

                    n.charDialogue.origHead = choice1;

                }

                else if(head.answer == "The words don't even reach his ears before the great man falls to the ground, dead. You're shake as you walk over to him and clutch one of the medals that lie on his chest. You feel like you should be feeling something, regret, remorse, anger, fear, but the most frightening thing at all is that there really isn't much there. You were comfortable doing this in the name of a cause.\n\nThe medal comes loose with one sharp pull. It's time to go back to Nechaev." || head.answer == "The words don't even reach his ears before the great man falls to the ground, dead. You're shake as you walk over to him and clutch one of the medals that lie on his chest. You feel like you should be feeling something, regret, remorse, anger, fear, but the most frightening thing at all is that there really isn't much there. You were comfortable doing this in the name of a cause.\n\nThe medal comes loose with one sharp pull. It is time to go back to Nechaev")
                {

                    Item medal = new Item();
                    List<String> medalNames = new List<String> { "Tsar's Medal", "medal", "the medal", "tsars medal", "royal medal" };
                    medal.initItem(medalNames, 12, "A regal military metal, given for bravery. It's covered in blood.");
                    Game.player.inventory.Add(medal);

                    Character n = null;
                    Choice choice1 = null;
                    Choice choice2 = null;

                    Character a = null;
                    Choice choicea1 = null;

                    foreach(Character i in Game.allChar)
                    {
                        if(i.charID == 8)
                        { 
                            n = i;
                        }

                        if(i.charID == 5)
                        {
                            a = i;
                        }
                    }

                    foreach(Choice i in Game.allChoices) /// Gets the choices we're trying to work with from the list of all choices. Kind of annoying, so maybe find another way to do it.
                    {
                        if(i.answer == "He sees you approach and greets you warmly.\n'Ah brother, is the task completed yet?'")
                        {
                            choice1 = i;
                        }

                        else if(i.answer == "He's dead.\nYou can't talk to dead people.")
                        {  
                            choicea1 = i;
                        }

                        else if(i.answer == "His eyes glow with energy as his trembling hands take the gift.\n'My, my, look at it, look at it! Our destiny, finally fufilled. You truly are a brother of the Revolution.'\nHe and the woman who have gathered at his side beam at you (although he's happier then they are).\n'Well, I think it's time you get your reward. Thank you for fulfilling your destiny, Iosef.'\n\nYou wonder why he called you that when your vision goes dark.\n\nAnd suddenly, you're awake.")
                        {
                            choice2 = i;
                           
                        }

                    }

                    n.charDialogue.Connect(choice1, choice2, "Yes it is. Here you go. (Give Him the Medal)");

                    a.charDialogue.origHead = choicea1;

                }





                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                if (head.choices.Count == 0) /// If you don't have any choices left, break the loop.
                {
                    break;
                }

                for (int i = 0; i < head.choices.Count; i++)
                {
                    Console.WriteLine($"{count}. {head.connectionStrings[i]}");
                    count++;
                }
      
                while(true)
                {
                    try
                    {
                        int input = int.Parse(Console.ReadLine());
                        head = head.choices[input - 1];
                        break;
                    }

                    catch (Exception)
                    {
                    Console.WriteLine("That's not a valid choice.");
                    }
                }
            }
        }

        public bool Connect(Choice c1, Choice c2, string connection) 
        {
            if (c1.choices.Contains(c2))
            {
                return false;
            }

            c1.choices.Add(c2);
            c1.connectionStrings.Add(connection);
            return true;
        }

        public bool UnConnect(Choice c1, Choice c2, string connection) 
        {
            if (!c1.choices.Contains(c2))
            {
                return false;
            }

            c1.choices.Remove(c2);
            c1.connectionStrings.Remove(connection);
            return true;
        }

        public class Choice
        {
            public string answer;

            public int actionID;
            public List<Choice> choices;
            public List<string> connectionStrings;
            public Choice(string answer)
            {
                this.answer = answer;
                choices = new List<Choice>();
                connectionStrings = new List<string>();
            }
        }
    }
}